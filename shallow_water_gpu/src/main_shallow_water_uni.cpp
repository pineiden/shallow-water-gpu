#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo
#include "ShallowWater/topografia_uni.hpp"
#include "ShallowWater/curva_gaussiana_uni.hpp"
#include "ShallowWater/unidimensional.hpp"


namespace fs = std::filesystem;
using namespace std;

int main(int argc, char *argv[]){
  float step = atof(argv[1]);
  float delta_time = atof(argv[2]);
  float amplitud = atof(argv[3]);
  fs::path path = argv[4];
  int iterations = atof(argv[5]);
  float h_base = atof(argv[6]);

  std::cout<<"Topografia "<<path<<std::endl;
  ShallowWaterUnidim sh0(step, delta_time, path, amplitud, h_base);
  std::cout<<sh0<<std::endl;
  sh0.showTopografia(true);
  sh0.showTopografia(false);
  sh0.showEstimulo();
  cout<<"END SH0"<<endl;

  float *nh = new float[sh0.getSize()];
  std::cout<<sh0<<std::endl;
  sh0.newHeights(nh);

  ShallowWaterUnidim next_step = ShallowWaterUnidim(nh, sh0);
  std::vector<float> new_u;
  sh0.newVelocity(next_step, new_u);

  auto start = std::chrono::high_resolution_clock::now();

  for (int i=0;i<iterations;i++){
	cout<<"END SH next i:"<<i<<endl;
	ShallowWaterUnidim sh1 = sh0.next();
	std::cout<<sh1<<std::endl;
	sh0 = sh1;
  }

  // Captura el tiempo de finalización
  auto end = std::chrono::high_resolution_clock::now();

  // Calcula el tiempo transcurrido en segundos
  std::chrono::duration<double> duration = end - start;
  double seconds = duration.count();

  // Imprime el tiempo transcurrido
  std::cout << "Tiempo transcurrido: " << seconds << " segundos" << std::endl;

  return 0;
};
