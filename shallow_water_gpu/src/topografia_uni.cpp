#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include "ShallowWater/topografia_uni.hpp"

namespace fs = std::filesystem;
using namespace std;


class ZeroDivException : public runtime_error {
public:
    // Defining constructor of class Exception
    // that passes a string message to the runtime_error class
    ZeroDivException()
        : runtime_error("Math error: Attempted to divide by Zero\n")
    {
    }
};


std::vector<std::pair<float, float>> readCSV(const fs::path& filePath) {
    std::vector<std::pair<float, float>> data;

    if (!fs::exists(filePath)) {
        throw std::runtime_error("El archivo no existe.");
    }

    std::ifstream file(filePath);
    if (!file.is_open()) {
        throw std::runtime_error("Error al abrir el archivo.");
    }

    std::string line;
    // Ignoramos la primera línea, que contiene las cabeceras (valor1, valor2)
    std::getline(file, line);

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        float valor1, valor2;
        char comma; // Para descartar la coma que separa los valores

        // Leemos los valores como float separados por coma en cada línea
        if (iss >> valor1 >> comma >> valor2) {
            data.emplace_back(valor1, valor2);
        } else {
            std::cerr << "Error al leer una línea del archivo." << std::endl;
        }
    }

    return data;
}

// struct pair_hash {
//     template <class T1, class T2>
//     std::size_t operator () (const std::pair<T1,T2> &p) const {
//         auto h1 = std::hash<T1>{}(p.first);
//         auto h2 = std::hash<T2>{}(p.second);

//         // Mainly for demonstration purposes, i.e. works but is overly simple
//         // In the real world, use sth. like boost.hash_combine
//         return h1 ^ h2;  
//     }
// };

using TupleKey = std::pair<int, int>;

TopografiaUnidim::TopografiaUnidim(){
  /*
	Archivo con pares de valores
	posicion, altura
   */
  //pendientesMap = std::unordered_map<TupleKey, float,
  //pair_hash>();
}

TopografiaUnidim::TopografiaUnidim(std::vector<std::pair<float, float>> hp): height_position(hp) {
  size = height_position.size();	
}


TopografiaUnidim::TopografiaUnidim(fs::path path){
  /*
	Archivo con pares de valores
	posicion, altura
   */
  //pendientesMap = std::unordered_map<TupleKey, float, pair_hash>();
  height_position = readCSV(path);
  size = height_position.size();
}

std::vector<float> TopografiaUnidim::getX(){
 std::vector<float> x;
 for (int i=0;i<size;i++){
   float pos = height_position[i].first;
   x.push_back(pos);
 }
 return x;
}

float TopografiaUnidim::height(float x){
 int i=-2;
 int j=-2;
 int index = 0;
 for (unsigned int index=0; index < size; index++){
   if (x<height_position[index].first) {
	 i=-1;
	 j=0;
	 break;
   } else if (height_position[index].first<=x && x<height_position[index+1].first) {
	 i = index;
	 j = index + 1;
	 break;
   } else if (height_position[index].first<=x) {
	 i = size;
	 j = size + 1;
   }
 }
 return recta(i,j,x);
}

float TopografiaUnidim::pendiente(int i, int j){
 if (i==j){
   throw ZeroDivException();
 }
 if (i<=-1){
   i = 0;
   j = 1;
 }
 if (i>=size){
   i = size-1;
   j = size;
 }
 float m = (height_position[j].second - height_position[i].second)/(height_position[j].first-height_position[i].first);
 return m;
}

float TopografiaUnidim::recta(int i, int j, float x){
 float m = getPendiente(i,j);
 float c = height_position[i].second - m * height_position[i].first;
 return m*x  + c;
}

float TopografiaUnidim::first(){
 return height_position[0].first;
}

float TopografiaUnidim::last(){
 return height_position[size-1].first;
}

void TopografiaUnidim::addPendiente(int i, int j, float pendiente) {
   TupleKey key = std::make_pair(i, j);
   pendientesMap[key] = pendiente;
}

// Obtener la pendiente entre las tuplas (i, j)
float TopografiaUnidim::getPendiente(int i, int j) {
   TupleKey key = std::make_pair(i, j);
   auto it = pendientesMap.find(key);
   if (it != pendientesMap.end()) {
	   return it->second;
   } else {
	 float m = pendiente(i,j);
	 addPendiente(i, j, m);
	   // Manejar el caso si la pendiente no está presente en el mapa
	 return m; // Puedes devolver un valor predeterminado o lanzar una excepción, según tus necesidades.
   }
}


std::ostream& operator<<(
						std::ostream& os, 
						const TopografiaUnidim& v) {

 os << "Topografia x,y"<<endl;
 for (unsigned int index=0; index < v.size; index++){
   os <<  v.height_position[index].first << "," << v.height_position[index].second<<endl;
 }
 return os;
}

TopografiaUnidim TopografiaUnidim::genera_topografia(float step) {
 std::vector<std::pair<float, float>> hp;
 // desde x_min sumar step to step hasta length
 float x_min = first();
 float x_max = last();
 float x_val = x_min;
 float y_val = 0;
 while (x_val <= x_max +step) {
	y_val = height(x_val);
	hp.emplace_back(x_val, y_val);
	x_val += step;
 }
 return TopografiaUnidim(hp);
}

int TopografiaUnidim::length() {
 return this->height_position.size();
}

float TopografiaUnidim::getValue(int i){
 return height_position[i].second;
}


float TopografiaUnidim::maxZ() const {
   if (height_position.empty()) {
	   // Si el vector está vacío, devuelve un valor predeterminado o lanza una excepción si es apropiado.
	   // Aquí devolveré un valor predeterminado de 0, pero puedes adaptarlo según tus necesidades.
	   return 0.0;
   }

   // Usamos la función std::max_element de la biblioteca algorithm para encontrar el valor máximo en el vector.

	// Encontrar el máximo valor según el segundo valor de cada pair
	auto maxElement = std::max_element(height_position.begin(), height_position.end(),
		[](const std::pair<int, float>& a, const std::pair<int, float>& b) {
			return a.second < b.second;
		}
	);

	if (maxElement != height_position.end()) {
	  return maxElement->second;
	} else {
	  return 0.0;
	}
}



