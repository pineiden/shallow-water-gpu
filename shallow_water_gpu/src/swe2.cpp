#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <chrono> // Para trabajar con tiempo

using namespace std;
class Simulacion {
  private:
  int L;
  int W;
  int iterations;
  float time_data;
  float time_saving;
  float time_matrix;
  public:
  Simulacion(int L,int I): L(L), iterations(I){
  }

  void setW(float w){
	W=w;
  }
  void setTD(float time){
	time_data = time;
  }
  void setTS(float time){
	time_saving = time;
  }
  void setTM(float time){
	time_matrix = time;
  }

  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const Simulacion& s) {
	os <<s.iterations<<","<<s.L<<","<<s.W<<","<<s.time_data<<","<<s.time_saving<<","<<s.time_matrix<<endl;
	return os;
  }

  string head() const{
	return "iterarions,length,width,time_data,time_saving,time_matrix";
  }



Simulacion& operator=(const Simulacion& other) {
	// Verifica si no se está asignando a sí mismo
	if (this != &other) {
		// Realiza la asignación de los miembros
		L = other.L;
		W = other.W;
		iterations = other.iterations;
		time_data = other.time_data;
		time_saving = other.time_saving;
		time_matrix = other.time_matrix;
	}
	// Devuelve el propio objeto para permitir asignaciones encadenadas
	return *this;
  }
};

string shallow_water_1d_test(int nx_init = 40, int nt_init = 500, double distance = 1, double g_init = 9.8);
void shallow_water_1d(int nx, int nt, double x_length, double t_length, double g, double** h_array, double** uh_array, double* x, double* t);
string shallow_water_1d_sim(int nx_init, int nt_init, double distance, double g_init, Simulacion& sim);
void boundary_conditions(int nx, int nt, double* h, double* uh, double t);
void initial_conditions(int nx, int nt, double* h, double* uh, double* x);
std::string write_data(int nx, int nt, double* x, double* t, double** h_array, double** uh_array);

int main_test() {
    std::cout << "Implementation of Shallow Water Equations in 1D" << std::endl;
    shallow_water_1d_test();
    return 0;
}
/*
nx : vertices totales en largo L
nt : frames
distance : step entre nodo
g_init: valor de gravedad
*/

string shallow_water_1d_test(int nx_init, int nt_init, double distance, double g_init) {
    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D_TEST:" << std::endl;
    std::cout << "  Compute a solution of the discrete shallow water equations" << std::endl;
    std::cout << "  over a 1-dimensional domain." << std::endl;

    int nx = nx_init;
    int nt = nt_init;
    double x_length = distance;
    double t_length = 0.5;
    double g = g_init;

    double** h_array = new double*[nx];
    double** uh_array = new double*[nx];
    double* x = new double[nx];
    double* t = new double[nt + 1];
    for (int i = 0; i < nx; i++) {
        h_array[i] = new double[nt + 1];
        uh_array[i] = new double[nt + 1];
    }

    shallow_water_1d(nx, nt, x_length, t_length, g, h_array, uh_array, x, t);

    double x_min = *std::min_element(x, x + nx);
    double x_max = *std::max_element(x, x + nx);

    double h_min = 0.0;
    double h_max = 0.0;
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            h_max = std::max(h_max, h_array[i][j]);
        }
    }

    double uh_min = 0.0;
    double uh_max = 0.0;
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            uh_min = std::min(uh_min, uh_array[i][j]);
            uh_max = std::max(uh_max, uh_array[i][j]);
        }
    }

    string filename = write_data(nx, nt, x, t, h_array, uh_array);

    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D_TEST:" << std::endl;
    std::cout << "  Normal end of execution." << std::endl;

    for (int i = 0; i < nx; i++) {
        delete[] h_array[i];
        delete[] uh_array[i];
    }
    delete[] h_array;
    delete[] uh_array;
    delete[] x;
    delete[] t;
	return filename;
}

/* simulacion */


string shallow_water_1d_sim(int nx_init, int nt_init, double distance, double g_init, Simulacion& sim) {
    auto start = std::chrono::high_resolution_clock::now();

    int nx = nx_init;
    int nt = nt_init;
    double x_length = distance;
    double t_length = 0.5;
    double g = g_init;

    double** h_array = new double*[nx];
    double** uh_array = new double*[nx];
    double* x = new double[nx];
    double* t = new double[nt + 1];
    for (int i = 0; i < nx; i++) {
        h_array[i] = new double[nt + 1];
        uh_array[i] = new double[nt + 1];
    }

    shallow_water_1d(nx, nt, x_length, t_length, g, h_array, uh_array, x, t);

    double x_min = *std::min_element(x, x + nx);
    double x_max = *std::max_element(x, x + nx);

    double h_min = 0.0;
    double h_max = 0.0;
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            h_max = std::max(h_max, h_array[i][j]);
        }
    }

    double uh_min = 0.0;
    double uh_max = 0.0;
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            uh_min = std::min(uh_min, uh_array[i][j]);
            uh_max = std::max(uh_max, uh_array[i][j]);
        }
    }
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration = end - start;
	double seconds = duration.count();
	sim.setTD(seconds);

    auto start_b = std::chrono::high_resolution_clock::now();
    std::string filename = write_data(nx, nt, x, t, h_array, uh_array);
	auto end_b = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration_b = end_b - start_b;
	seconds = duration_b.count();
	sim.setTS(seconds);

    for (int i = 0; i < nx; i++) {
        delete[] h_array[i];
        delete[] uh_array[i];
    }
    delete[] h_array;
    delete[] uh_array;
    delete[] x;
    delete[] t;
	return filename;
}

/* end simulacion */






void shallow_water_1d(int nx, int nt, double x_length, double t_length, double g, double** h_array, double** uh_array, double* x, double* t) {
    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D:" << std::endl;
    std::cout << "  Compute a solution of the discrete shallow water equations" << std::endl;
    std::cout << "  over a 1-dimensional domain." << std::endl;

    double* h = new double[nx];
    double* uh = new double[nx];
    double* hm = new double[nx - 1];
    double* uhm = new double[nx - 1];

    for (int i = 0; i < nx; i++) {
        x[i] = (double)i * x_length / (double)(nx - 1);
    }
    for (int i = 0; i < nt + 1; i++) {
        t[i] = (double)i * t_length / (double)nt;
    }
    double dx = x_length / (double)(nx - 1);
    double dt = t_length / (double)nt;

    initial_conditions(nx, nt, h, uh, x);

    boundary_conditions(nx, nt, h, uh, t[0]);

    for (int i = 0; i < nx; i++) {
        h_array[i][0] = h[i];
        uh_array[i][0] = uh[i];
    }

    for (int it = 1; it < nt + 1; it++) {
        for (int i = 0; i < nx - 1; i++) {
            hm[i] = (h[i] + h[i + 1]) / 2.0 - (dt / 2.0) * (uh[i + 1] - uh[i]) / dx;
            uhm[i] = (uh[i] + uh[i + 1]) / 2.0 - (dt / 2.0) * (uh[i + 1] * uh[i + 1] / h[i + 1] + 0.5 * g * h[i + 1] * h[i + 1] - uh[i] * uh[i] / h[i] - 0.5 * g * h[i] * h[i]) / dx;
        }

        for (int i = 1; i < nx - 1; i++) {
            h[i] = h[i] - dt * (uhm[i] - uhm[i - 1]) / dx;
            uh[i] = uh[i] - dt * (uhm[i] * uhm[i] / hm[i] + 0.5 * g * hm[i] * hm[i] - uhm[i - 1] * uhm[i - 1] / hm[i - 1] - 0.5 * g * hm[i - 1] * hm[i - 1]) / dx;
        }

        boundary_conditions(nx, nt, h, uh, t[it]);

        for (int i = 0; i < nx; i++) {
            h_array[i][it] = h[i];
            uh_array[i][it] = uh[i];
        }
    }

    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D:" << std::endl;
    std::cout << "  Normal end of execution." << std::endl;

    delete[] h;
    delete[] uh;
    delete[] hm;
    delete[] uhm;
}

void boundary_conditions(int nx, int nt, double* h, double* uh, double t) {
    int bc = 1;

    if (bc == 1) {
        h[0] = h[nx - 2];
        h[nx - 1] = h[1];
        uh[0] = uh[nx - 2];
        uh[nx - 1] = uh[1];
    }
    else if (bc == 2) {
        h[0] = h[1];
        h[nx - 1] = h[nx - 2];
        uh[0] = uh[1];
        uh[nx - 1] = uh[nx - 2];
    }
    else if (bc == 3) {
        h[0] = h[1];
        h[nx - 1] = h[nx - 2];
        uh[0] = -uh[1];
        uh[nx - 1] = -uh[nx - 2];
    }
}

void initial_conditions(int nx, int nt, double* h, double* uh, double* x) {
    for (int i = 0; i < nx; i++) {
        h[i] = 2.0 + std::cos(2.0 * M_PI * x[i]);
        uh[i] = 0.0;
    }
}

std::string write_data(int nx, int nt, double* x, double* t, double** h_array, double** uh_array) {
    std::string filename = "coordenadas_"+std::to_string(nx)+"_"+std::to_string(nt)+".txt";
    std::ofstream file_h(filename);
    // std::ofstream file_x("sw1d_x.txt");
    // std::ofstream file_t("sw1d_t.txt");
    // std::ofstream file_uh("sw1d_uh.txt");

    // for (int i = 0; i < nx; i++) {
    //     file_x << x[i] << std::endl;
    // }
    // for (int i = 0; i < nt + 1; i++) {
    //     file_t << t[i] << std::endl;
    // }
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            file_h << h_array[i][j] << " ";
        }
        file_h << std::endl;
    }
    // for (int i = 0; i < nx; i++) {
    //     for (int j = 0; j < nt + 1; j++) {
    //         file_uh << uh_array[i][j] << " ";
    //     }
    //     file_uh << std::endl;
    // }

    file_h.close();
    // file_x.close();
    // file_t.close();
    // file_uh.close();

    // std::cout << std::endl;
    // std::cout << "  X  values saved in file \"sw1d_x.txt\"" << std::endl;
    // std::cout << "  T  values saved in file \"sw1d_t.txt\"" << std::endl;
    // std::cout << "  H  values saved in file \"sw1d_h.txt\"" << std::endl;
    // std::cout << "  UH values saved in file \"sw1d_uh.txt\"" <<
    // std::endl;
	return filename;
}
