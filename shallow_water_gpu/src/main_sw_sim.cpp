#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <thread>
#include <chrono> // Para trabajar con tiempo

#ifdef _WIN32
#include "../include/ShallowWater/plane.hpp"
#endif

#ifdef linux
#include "ShallowWater/plane.hpp"

#endif 

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "ShallowWater/mesh_gl_uni.hpp"
#include "ShallowWater/shaders.hpp"
// #include "ShallowWater/topografia_uni.hpp"
// #include "ShallowWater/curva_gaussiana_uni.hpp"
// #include "ShallowWater/unidimensional.hpp"
#include "ShallowWater/mesh_gl_uni.hpp"
#include "swe2.cpp"

#define PI 3.141592653589793 /* pi value */ 

namespace fs = std::filesystem;
using namespace glm;
GLFWwindow* window;


void guardarVectorEnCSV(
const std::vector<float>& vector, 
const std::string& nombreArchivo) {
    std::ofstream archivo(nombreArchivo, std::ios::app); // Abrir el archivo en modo de adjuntar datos

    if (archivo.is_open()) {
        for (size_t i = 0; i < vector.size(); ++i) {
            archivo << vector[i]; // Escribir el valor del vector
            if (i < vector.size() - 1) {
                archivo << ","; // Agregar una coma después del valor, excepto para el último valor de la fila
            }
        }
        archivo << "\n"; // Agregar un salto de línea al final de la fila
        archivo.close(); // Cerrar el archivo
        std::cout << "Vector guardado en el archivo: " << nombreArchivo << std::endl;
    } else {
        std::cerr << "No se pudo abrir el archivo: " << nombreArchivo << std::endl;
    }
}


void initGl() {
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 
}


// fn para cargar shader
GLuint cargarShader(GLenum tipoShader, const char* shaderSource)
{
    GLuint shader = glCreateShader(tipoShader);
    glShaderSource(shader, 1, &shaderSource, nullptr);
    glCompileShader(shader);

    // Verificar errores de compilación del shader
    GLint estadoCompilacion;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &estadoCompilacion);
    if (estadoCompilacion == GL_FALSE)
    {
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> logMensaje(logLength);
        glGetShaderInfoLog(shader, logLength, nullptr, logMensaje.data());
        std::cerr << "Error al compilar shader: " << logMensaje.data() << std::endl;
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}



std::vector<float> replicateHeight(
	std::vector<float>h, 
	unsigned int W){
  std::vector<float> new_h;
  float val;
  for (int i = 0; i < h.size(); i++){
	val = h[i];
	for (int j=0;j<W;j++){
	  new_h.push_back(val);
	}
  }
  return new_h;  
}



#include <cmath>


std::vector<float> sinMesh(
	unsigned int L, 
	unsigned int W,
	float A, int counter
){
  std::vector<float> new_h;
  float val;
  for (int i = 0; i < L; i++){

	val = A*sin((((float)i)/(2*PI))*((float)counter));
	for (int j=0;j<W;j++){
	  new_h.push_back(val);
	}

  }
  return new_h;  
}


bool mouseLeftPressed = false;
double lastMouseX = 0.0;
double lastMouseY = 0.0;

float fov = 45.0f;
glm::mat4 View = glm::lookAt(glm::vec3(20,8,10), glm::vec3(0,0,0), glm::vec3(0,1,0));


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{

    //cout<<"Scroll event wheel"<<endl;
    fov -= (float)yoffset;
    if (fov < 1.0f)
        fov = 1.0f;
    if (fov > 45.0f)
        fov = 45.0f; 
}


void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    //cout<<"Mouse button event "<<endl;

    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            mouseLeftPressed = true;
            glfwGetCursorPos(window, &lastMouseX, &lastMouseY);
        } else if (action == GLFW_RELEASE) {
            mouseLeftPressed = false;
        }
    }
}

void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    //cout<<"Cursor position event and mouse "<<endl;

    if (mouseLeftPressed) {
        double deltaX = xpos - lastMouseX;
        double deltaY = ypos - lastMouseY;

        // Adjust the view matrix to move the camera based on mouse movement
        // You can adjust these values to control the sensitivity of the mouse movement
        float rotationSpeed = 0.1f;
        View = glm::rotate(View, static_cast<float>(deltaX) * rotationSpeed, glm::vec3(0, 1, 0));
        View = glm::rotate(View, static_cast<float>(deltaY) * rotationSpeed, glm::vec3(1, 0, 0));

        lastMouseX = xpos;
        lastMouseY = ypos;
    }
}




// Función para guardar una serie de instancias en un archivo CSV
void guardarEnCSV(const std::vector<Simulacion>& instancias, const std::string& nombreArchivo) {
    std::ofstream archivo(nombreArchivo);

    if (archivo.is_open()) {
	    archivo << instancias[0].head()<<"\n";
        for (const Simulacion& instancia : instancias) {
            archivo << instancia << "\n";
        }
        archivo.close();
        std::cout << "Instancias guardadas en el archivo: " << nombreArchivo << std::endl;
    } else {
        std::cerr << "No se pudo abrir el archivo: " << nombreArchivo << std::endl;
    }
}



/* end resize callback*/
int main(int argc, char *argv[]){
  //test();
  // largo vendrá dado desde la topografía lineal
  const int largo_min = atof(argv[1]);
  const int largo_max = atof(argv[2]);
  int largo = largo_min;
  const int ancho_min = atof(argv[3]);
  const int ancho_max = atof(argv[4]);
  const int paso = atof(argv[5]);
  int ancho = ancho_min;

  const float step = atof(argv[6]);
  const int iterations = atof(argv[7]);
  const int sleep = atof(argv[8]);
  
  std::vector<Simulacion> simulaciones;
  std::vector<Mesh> mechas;
  std::vector<string> files;
  double gravedad = 9.8;

  /* creando las simulaciones para diferentes L*/
  for (int l=largo_min;l<=largo_max;l+=paso){
	  Simulacion sim(l,iterations);
	  string filename = shallow_water_1d_sim(l,iterations,step,gravedad,sim);
	  simulaciones.push_back(sim);
	  files.push_back(filename);
	  cout<<"Sim time -> "<<sim<<endl;
  }


  std::cout <<"Shallow Water GPU"<<std::endl;
  Shaders shaders("mesh_gl/vertex.glsl", "mesh_gl/fragment.glsl");
  shaders.show();

  if( !glfwInit() )
  {
	  fprintf( stderr, "Failed to initialize GLFW\n" );
	  getchar();
	  return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed

  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  window = glfwCreateWindow( 1024, 768, "Shallow Water 1-dim", NULL, NULL);
  if( window == NULL ){
	  fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
	  getchar();
	  glfwTerminate();
	  return -1;
  }
  glfwMakeContextCurrent(window); // Initialize GLEW
  glewExperimental=true; // Needed in core profile
  if (glewInit() != GLEW_OK) {
	  fprintf(stderr, "Failed to initialize GLEW\n");
	  return -1;
  }

  // capture window

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  // Enable depth test
  glEnable(GL_DEPTH_TEST);
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc(GL_LESS); 

  /*load shaders*/
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // Create and compile our GLSL program from the shaders
  const char *c_vertexShader = shaders.vertex().c_str();
  const char *c_fragmentShader = shaders.fragment().c_str();
  GLuint vertexShader = cargarShader(GL_VERTEX_SHADER, c_vertexShader);
  GLuint fragmentShader = cargarShader(GL_FRAGMENT_SHADER, c_fragmentShader);
  
  // en esto se toma la iteración, es un Guint
   GLuint shaderProgram = glCreateProgram();
   // Adjunta los shaders al programa
   glAttachShader(shaderProgram, vertexShader);
   glAttachShader(shaderProgram, fragmentShader);

   // Enlaza los shaders
   glLinkProgram(shaderProgram);

	// Enlaza los shaders
	glLinkProgram(shaderProgram);

	// Verifica errores de enlace
	GLint estadoEnlace;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &estadoEnlace);
	if (estadoEnlace == GL_FALSE)
	{
		GLint logLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> logMensaje(logLength);
		glGetProgramInfoLog(shaderProgram, logLength, nullptr, logMensaje.data());
		std::cerr << "Error al enlazar el programa de shader: " << logMensaje.data() << std::endl;
	}



	// Valida el programa
	glValidateProgram(shaderProgram);

	// Utiliza el programa en tu código OpenGL
	// creamos las variables  y matrices a usar:

	// se obtiene el id de la variable uniform en vertex
	GLuint MatrixID = glGetUniformLocation(shaderProgram, "MVP");
	// se define una matriz de proyección:
	// L = M = 1.0, N: nro celdas, step = 0.1

  for (int l=largo_min;l<=largo_max;l+=paso){
	  Mesh mesh(largo, ancho, step); 
	  mesh.initializeBuffers();
	  mesh.setupVertexAttribs(shaderProgram);	  
	  mechas.push_back(mesh);
  }


    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetScrollCallback(window, scroll_callback);


	glm::mat4 Projection = glm::perspective(glm::radians(45.0f),4.0f/3.00f,0.1f,100.0f);

	glm::mat4 View = glm::lookAt(glm::vec3(20,8,10), glm::vec3(0,0,0), glm::vec3(0,1,0));


	// Model matrix :: identity matrix
	glm::mat4 Model = glm::mat4(1.0f);
	
	// modelviewprojection :: multiplicacion de las 3 matrices

	glm::mat4 MVP = Projection * View * Model;

	// ahora lo mismo, definir los vértices del triangulo

	static const GLfloat g_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 0.0f,  1.0f, 0.0f,
	};

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);



  int counter=0;
  /*end load shaders*/

  /* lectura de coordenadas o generación de ma*/

  std::vector<std::vector<std::vector<float>>> simulacionesMatrix;

  for (int f=0;f<files.size();f++) {
	

	string filename = files[f];
	Simulacion sim=simulaciones[f];
	cout<<"file: "<<filename <<" sim: "<<sim<<endl;
	/* read the file and build the matrix */

	
    auto start = std::chrono::high_resolution_clock::now();

	std::ifstream file(filename);
	if (!file.is_open()) {
            fprintf(stderr, "Failed to open file: %s\n", filename.c_str());
            return 0;
        }

    std::string line;
    std::vector<std::vector<float>> vertexValues;  // Almacenar los valores de los vértices

	/* read the file all iterations */
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<float> rowValues;
        float value;

        while (iss >> value) {
            rowValues.push_back(value);
        }

        if (!rowValues.empty()) {
            vertexValues.push_back(rowValues);
        }
    }

    file.close();

	/*
	  tomar matriz de iteraciones-simulacion
	 */
    std::vector<std::vector<float>> columnValues;
    size_t num_rows = vertexValues.size();
    size_t num_cols = vertexValues[0].size();


	/*
	  hacer que columnValues tenga un tamaño definido por num_rows (W)
	 */
    columnValues.resize(num_cols, std::vector<float>(num_rows));
    for (size_t i = 0; i < num_rows; i++) {
        for (size_t j = 0; j < num_cols; j++) {
            columnValues[j][i] = vertexValues[i][j];
        }
    }

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> duration = end - start;
	double seconds = duration.count();
	sim.setTM(seconds);
	simulacionesMatrix.push_back(columnValues);
  }

  /**/
	/*
	  guardar alturas
	 */
    //std::ofstream archivo("alturas.txt", std::ios::app); // Abrir el archivo en modo de adjuntar datos
  int pos = 0;

  guardarEnCSV(simulaciones, "simulaciones.csv");
   

  //archivo.close(); // Cerrar el archivo

  // Cleanup VBO and shader
  glDeleteBuffers(1, &vertexbuffer);
  glDeleteProgram(shaderProgram);
  glDeleteVertexArrays(1, &VertexArrayID);

  // Close OpenGL window and terminate GLFW
  glfwTerminate();

  return 0;
}
