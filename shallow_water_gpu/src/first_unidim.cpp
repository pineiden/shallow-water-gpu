#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo


namespace fs = std::filesystem;
using namespace std;

/*

Omega = (0,L) espacio x
(0,T) tiempo
N_t timestep
delta_t :: valor set
n in {0,...,N_t}
t^n = n * delta_t tiempo discretizado
Omega : N_x celdas

borde izq, centro, borde derecho
x_{i-1/2}, x_{i}, x_{i+i/2}

h: water height
z: topografía discretizada en el centro de cada celda
u: discretizada en las interfaces entre celdas
h(x_i,t_upN)-> h_i_upN
u(x_i,t_upN) -> u_i_upN

z(x_i) -> z_i

Además conexión entre los bordes: 
u_{1/2}_upN = u_{Nx+1/2}_upN
h_{o}_upN = h_{Nx}_upN

M = {1,....,Nx}
*/


struct OutOfCell: std::logic_error {
  using logic_error::logic_error;
  OutOfCell(): logic_error("Index out of available cells"){}


};

int upperLength(float length, float step){
  return (int)(length / step);
};


enum Posicion {
  left=1, 
  center=2, 
  right=3};

class ZeroDivException : public runtime_error {
public:
    // Defining constructor of class Exception
    // that passes a string message to the runtime_error class
    ZeroDivException()
        : runtime_error("Math error: Attempted to divide by Zero\n")
    {
    }
};



int obtenerNumeroAleatorio(int N) {
    // Creamos un generador de números aleatorios utilizando el motor mersenne_twister_engine
    // como generador de números pseudoaleatorios.
    std::random_device rd;
    std::mt19937 gen(rd());

    // Creamos una distribución uniforme que generará números enteros en el rango de 0 a N-1.
    std::uniform_int_distribution<> distribucion(0, N - 1);

    // Generamos el número aleatorio utilizando la distribución y el generador.
    int numeroAleatorio = distribucion(gen);

    return numeroAleatorio;
}


std::vector<std::pair<float, float>> readCSV(const fs::path& filePath) {
    std::vector<std::pair<float, float>> data;

    if (!fs::exists(filePath)) {
        throw std::runtime_error("El archivo no existe.");
    }

    std::ifstream file(filePath);
    if (!file.is_open()) {
        throw std::runtime_error("Error al abrir el archivo.");
    }

    std::string line;
    // Ignoramos la primera línea, que contiene las cabeceras (valor1, valor2)
    std::getline(file, line);

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        float valor1, valor2;
        char comma; // Para descartar la coma que separa los valores

        // Leemos los valores como float separados por coma en cada línea
        if (iss >> valor1 >> comma >> valor2) {
            data.emplace_back(valor1, valor2);
        } else {
            std::cerr << "Error al leer una línea del archivo." << std::endl;
        }
    }

    return data;
}

struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        // Mainly for demonstration purposes, i.e. works but is overly simple
        // In the real world, use sth. like boost.hash_combine
        return h1 ^ h2;  
    }
};

using TupleKey = std::pair<int, int>;

class TopografiaUnidim {
private:
  fs::path path;
  std::vector<std::pair<float, float>> height_position;
  std::unordered_map<TupleKey, float, pair_hash> pendientesMap;
  size_t size;
public:
   TopografiaUnidim(){
	 /*
	   Archivo con pares de valores
	   posicion, altura
	  */
	 //pendientesMap = std::unordered_map<TupleKey, float,
	 //pair_hash>();
   }

  TopografiaUnidim(std::vector<std::pair<float, float>> hp): height_position(hp) {
	 size = height_position.size();	
  }


   TopografiaUnidim(fs::path path){
	 /*
	   Archivo con pares de valores
	   posicion, altura
	  */
	 //pendientesMap = std::unordered_map<TupleKey, float, pair_hash>();
	 height_position = readCSV(path);
	 size = height_position.size();
   }

  std::vector<float> getX(){
	std::vector<float> x;
	for (int i=0;i<size;i++){
	  float pos = height_position[i].first;
	  x.push_back(pos);
	}
	return x;
  }

  float height(float x){
	int i=-2;
	int j=-2;
	int index = 0;
	for (unsigned int index=0; index < size; index++){
	  if (x<height_position[index].first) {
		i=-1;
		j=0;
		break;
	  } else if (height_position[index].first<=x && x<height_position[index+1].first) {
		i = index;
		j = index + 1;
		break;
	  } else if (height_position[index].first<=x) {
		i = size;
		j = size + 1;
	  }
	}
	return recta(i,j,x);
  }

  float pendiente(int i, int j){
	if (i==j){
	  throw ZeroDivException();
	}
	if (i<=-1){
	  i = 0;
	  j = 1;
	}
	if (i>=size){
	  i = size-1;
	  j = size;
	}
	float m = (height_position[j].second - height_position[i].second)/(height_position[j].first-height_position[i].first);
	return m;
  }

  float recta(int i, int j, float x){
	float m = getPendiente(i,j);
	float c = height_position[i].second - m * height_position[i].first;
	return m*x  + c;
  }

  float first(){
	return height_position[0].first;
  }

  float last(){
	return height_position[size-1].first;
  }

  void addPendiente(int i, int j, float pendiente) {
	  TupleKey key = std::make_pair(i, j);
	  pendientesMap[key] = pendiente;
  }

  // Obtener la pendiente entre las tuplas (i, j)
  float getPendiente(int i, int j) {
	  TupleKey key = std::make_pair(i, j);
	  auto it = pendientesMap.find(key);
	  if (it != pendientesMap.end()) {
		  return it->second;
	  } else {
		float m = pendiente(i,j);
		addPendiente(i, j, m);
		  // Manejar el caso si la pendiente no está presente en el mapa
		return m; // Puedes devolver un valor predeterminado o lanzar una excepción, según tus necesidades.
	  }
  }


  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const TopografiaUnidim& v) {

	os << "Topografia x,y"<<endl;
	for (unsigned int index=0; index < v.size; index++){
	  os <<  v.height_position[index].first << "," << v.height_position[index].second<<endl;
	}
	return os;
  }

  TopografiaUnidim genera_topografia(float step) {
	std::vector<std::pair<float, float>> hp;
	// desde x_min sumar step to step hasta length
	float x_min = first();
	float x_max = last();
	float x_val = x_min;
	float y_val = 0;
	while (x_val <= x_max +step) {
	   y_val = height(x_val);
       hp.emplace_back(x_val, y_val);
	   x_val += step;
	}
	return TopografiaUnidim(hp);
  }
  
  int length() {
	return this->height_position.size();
  }

  float getValue(int i){
	return height_position[i].second;
  }


  float maxZ() const {
	  if (height_position.empty()) {
		  // Si el vector está vacío, devuelve un valor predeterminado o lanza una excepción si es apropiado.
		  // Aquí devolveré un valor predeterminado de 0, pero puedes adaptarlo según tus necesidades.
		  return 0.0;
	  }

	  // Usamos la función std::max_element de la biblioteca algorithm para encontrar el valor máximo en el vector.

	   // Encontrar el máximo valor según el segundo valor de cada pair
	   auto maxElement = std::max_element(height_position.begin(), height_position.end(),
		   [](const std::pair<int, float>& a, const std::pair<int, float>& b) {
			   return a.second < b.second;
		   }
	   );

	   if (maxElement != height_position.end()) {
		 return maxElement->second;
	   } else {
		 return 0.0;
	   }
  }


};


class CurvaGaussiana {
private:
    int N; // Tamaño del array
    std::vector<float> datos; // Array de N floats para almacenar la curva gaussiana
    float amplitud; // Amplitud de onda

public:
    // Constructor que inicializa el array de N floats como una curva gaussiana con centro en i, desviación estándar de j celdas y amplitud de onda
    CurvaGaussiana(int size, int i, int j, float amplitud_onda) : amplitud(amplitud_onda) {
        N = size;

        // Calcular el centro de la curva gaussiana
        float centro = i;

        // Calcular la desviación estándar
        float desviacion = j;

        // Calcular la constante de normalización para la gaussiana
        float normalizacion = amplitud / (desviacion * std::sqrt(2.0f * M_PI));


        // Llenar el array con los valores de la curva gaussiana
        for (int k = 0; k < N; k++) {
            float distanciaAlCentro = k - centro;
            float val = normalizacion * std::exp(-0.5f * std::pow(distanciaAlCentro / desviacion, 2));
			datos.push_back(val);
        }
    }

  CurvaGaussiana(CurvaGaussiana* curva){
	N = curva->N;
	amplitud = curva->amplitud;	
  }


    void setCurva(std::vector<float>& new_datos){
	  datos = new_datos;
	}
 
    // Destructor para liberar la memoria del array
    CurvaGaussiana() {
    }

    // Destructor para liberar la memoria del array
    ~CurvaGaussiana() {
        //delete[] datos;
    }

    // Obtener el valor en una posición del array
    float getValor(int index) const {
        if (index >= 0 && index < N) {
            return datos[index];
        } else {
            throw std::out_of_range("Índice fuera del rango del array.");
        }
    }


  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const CurvaGaussiana& v) {

	os << "Curva Gaussiana x,y"<<endl;
	for (unsigned int index=0; index < v.N; index++){
	  os <<  index << "," << v.datos[index]<<endl;
	}
	return os;
  }


  float maxZ() const {
	  if (datos.empty()) {
		  // Si el vector está vacío, devuelve un valor predeterminado o lanza una excepción si es apropiado.
		  // Aquí devolveré un valor predeterminado de 0, pero puedes adaptarlo según tus necesidades.
		  return 0.0;
	  }

	  // Usamos la función std::max_element de la biblioteca algorithm para encontrar el valor máximo en el vector.
	  auto maxElement = std::max_element(datos.begin(), datos.end());

	  // Devolvemos el valor máximo encontrado
	  return *maxElement;
  }

};


// aceleracion gravitacional en la tierra m2/s
const float G = 9.80665;

class ShallowWaterUnidim{
private:
  float length;
  float step;
  float delta_time;
  int N;
  int Points;
  float *line;
  // base topografica es la fuente
  TopografiaUnidim base;
  // es el array de puntos registrados de malla con su altura topografica
  TopografiaUnidim topografia;
  // va a representar la velocidad en el punto i
  CurvaGaussiana estimulo;
  float h_base;
  bool partial;
public:
  ShallowWaterUnidim(
				   float step, 
				   float delta_time, 
				   fs::path path, 
				   float amplitud, 
				   float h_base=100.0):base(path){
	 this->delta_time = delta_time;
	 this->step = step;
	 this->topografia = base.genera_topografia(step);
	 // distancia de x_min a x_max + delta
	 this->length =  std::abs(topografia.last() - topografia.first());
	 // consider a position for interface and center
	 // left border : center : right border
	 this->N = topografia.length();
	 this->Points = 2*N+1;
	 this->line = new float[Points];
	 float max_topo = topografia.maxZ();

	 for (int index=0; index<Points;index++){
	   line[index] = max_topo + h_base;
	 }
	 // leer topografía
	 // calcular cada punto
	 int centro = obtenerNumeroAleatorio(Points);
	 int desviacion = obtenerNumeroAleatorio((int)Points/2);
	 // curva gaussiana: Points: 2*N + 1 (bordes y celdas)
	 estimulo = CurvaGaussiana(Points, centro, desviacion, amplitud);
	 partial = false;
   }
  

  /* constructor de build next u*/
  ShallowWaterUnidim(
					 float* new_h, 
					 ShallowWaterUnidim& before):partial(true) {
	line = new_h;
	estimulo = before.estimulo;
	base = before.base;
	topografia = before.topografia;
	h_base = before.h_base;
	step = before.step;
	delta_time = before.delta_time;
	N = before.N;
	length = before.length;
	Points = before.Points;
  }

  void setEstimulo(CurvaGaussiana* estimulo){
	estimulo = estimulo;
	partial = false;
  }


  void check_cell(int i) { 
	  if ( !(0<=i && i<N) ){
		throw OutOfCell();
	  }
  }

  int getCenter(int i){
	check_cell(i);
	int j = 2*i + 1;
	return j;
  }

  int getLeft(int i){
	check_cell(i);
	int j = 2*i;
	return j;
  }


  int getRight(int i){
	check_cell(i);
	int j = 2*i + 2;
	return j;
  }

  int  fixIndex(int i){ 
	return i;
  }

  ~ShallowWaterUnidim(){
  }

  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const ShallowWaterUnidim& v) {
	os << "ShallowWaterUnidim(L:" << v.length << ",step:" << v.step << ",N:" << v.N <<",Points:"<<v.Points<<",delta time:"<<v.delta_time<<")";
	return os;
  }

  float velocidad(int i){
	return estimulo.getValor(i);
  }

  float altura(int i){
	return line[i];
  }

  int getSize() {
	return Points;
  }

  float elapsed(int iteration){
	return (float)iteration * delta_time;
  }

  float z(int i){
	return this->topografia.getValue(i);
  }

  float u(int i){
	return this->estimulo.getValor(i);
  }

  float h_ipos_n(Posicion pos, int i){
	/*
	  Parte -B- de ecuación 1.7
	 */

	switch(pos) {
	  case Posicion::left: {
		float v = velocidad(i - 1);
		if (v>=0){
		  return altura(i + 1);
		} else {
		  return altura(i - 1);
		}
	  }
	  case Posicion::right: {
		// obtener en tiempo 'n'
		int iv = i;
		if (i==Points) {
		  iv = 0;
		} else {
		  iv = i + 1;
		}
		float v = velocidad(iv);
		if (v>=0){
		  return altura(i - 1);
		} else {
		  return altura(iv);
		}
	  }
	  default: cout<<"No classification for this position"<<endl; break;
	}
	return 0.0;
  }
  
  float q_ipos_n(Posicion pos, int i){
	/*

	  Parte -A- de ecuacion 1.7
	  Depende de -B-

	  i :: cell position in all array
	 */

	float _h_ipos_n = h_ipos_n(pos, i);
	float _u_ipos_n = 0.0;
	
	switch(pos) {
	  case Posicion::left: {
		// obtener en tiempo 'n'
		i = i - 1;
		if (i<0) {
		  i = Points - 1;
		}
		_u_ipos_n = velocidad(i);
		break;
	  }
	  case Posicion::right: {

		int iv = i;
		if (i==Points) {
		  iv = 0;
		} else {
		  iv = i + 1;
		}
		_u_ipos_n = velocidad(iv);
		break;
	  }
	  default: break;
	}

	return _h_ipos_n * _u_ipos_n;
  }

  float h_i_next(int center) {
	/*
	  Balance de masa (eq 1.6)
	 */
	Posicion left = Posicion::left;
	Posicion right = Posicion::right;
	float qr = q_ipos_n(right, center);
	float ql = q_ipos_n(left, center);
	return altura(center) + (delta_time/step) *  (qr-ql);
  }
  


  /*
	Ecuaciones para balance de masa (1.8)
   */

  float q_i_n(int i){
	/*
	  obtener el punto medio de q en celda i (el centro), dados los bordes
	 */
	Posicion left = Posicion::left;
	Posicion right = Posicion::right;
	

	float qr = q_ipos_n(right, i);
	float ql = q_ipos_n(left, i);

	return (0.5)*(qr+ql);
  }

  float uh_i_n(int i) {
	/*
	  Obtencion de la velocidad, en que dado el valor de q en el
	  centro (i), se obtiene el valor de la velocidad en un borde u
	  otro
	  u_hat_ en centro de celda, hay que tener el valor q_i
	 */
	//cout << "Index pre quin ->"<<i<<endl;
	float qin = q_i_n(i);

	if (qin>=0){
	  return velocidad(i - 1);

	}else {
	  //cout<<"Quin < 0"<<qin<< " i-> "<<i<<endl;
	  int iv = i;
	  if (i==Points) {
		iv = 0;
	  } else {
		iv = i + 1;
	  }
	  return velocidad(iv);
	}
  }

  float h_imid(int i, int i_next) {
	/*
	  Obtiene la altura en la interfaz entre las dos celdas
	 */

	if (i<0){
	  // se conecta al otro extremo
	  i = Points - 2;
	  i_next = 1;
	}
	float h_i_n = altura(i);

	float h_inext_n = altura(i_next);

	return (0.5) * (h_i_n + h_inext_n);
  }
  
  float u_i_next(int i_right, ShallowWaterUnidim& next_step){
	/*
	  Balance de momentum eq (1.8)
	  Despejando u_imid_next ->
	  Utiliza shallowwater cargado solo con la altura

	  i = {0...<N}
	 */ 


	int i_center = i_right  - 1;
	int i_next = i_right  + 1;

	// valor next para n+1
	// calcular de balance de masa:

	// cout<<"Calculando h_imid con next_step"<<endl;
	// cout<<*this<<endl;

	float _h_i_next = next_step.h_imid(i_center, i_next); // ok altura en interfaz


	//cout<<"Calculando h_imid_next ok"<<endl;

	// calcular los valores h_mid
	// toma h de la celda, y de la celda proxima
	// en array se salta 1, gracias a getCenter(i+1)
	float _h_i_n = h_imid(i_center, i_next); // ok altura en inferfaz

	// encontrar U_{i+1/2}_n

	// valor base velocidad:
	// en el borde a la derecha

	//cout<<"Obteniendo velocidad en borde derecho"<< i_right <<endl;
	float u_iright_n = u(i_right); // ok verificado



	//cout<<"Obteniendo factor base"<< i_right <<"*"<<_h_i_n<<endl;
	float left_factor_base  = u_iright_n * _h_i_n;

	/* calculo final 
	   de ecucación de balance de momentum
	 */ 

	// delta/x
	float alpha = delta_time / step;

	// próxima celda:
	// factor q * u (i+1) en siguiente celda

	//cout <<" Calculando q_i_n next cell "<<i_next<<endl;
	float q_inext_n = q_i_n(i_next);

	//cout <<" Calculando _uh_i_n next cell"<<i_next<<endl;
	float _uh_inext_n = uh_i_n(i_next);

	// factor q * u (i, n) en celda actual
	// cout <<" Calculando q_i_n center cell "<<i_center<<endl;
	float this_q_i_n = q_i_n(i_center);

	// cout <<" Calculando _uh_i_n center cell"<<i_next<<endl;
	float this_uh_i_n = uh_i_n(i_center);

	float diff_q = q_inext_n*_uh_inext_n - this_q_i_n * this_uh_i_n;
	// factores H
	// efecto grav y topografia:
	// elevacion en siguiente paso
	// usando la altura del siguiente paso en esa misma posicion
	float h_inext = next_step.altura(i_next);
	float h_i = next_step.altura(i_center);

	// h en celda interfaz a la dercha

	// factores de gravedad y topografia
	float efecto_grav_1 = (0.5) * G * (h_inext - h_i) * (h_inext + h_i);
	float efecto_grav_2 =  G * _h_i_next *(z(i_next) - z(i_center)); 
	float efecto_grav = efecto_grav_1 + efecto_grav_2;


	// factor del alfa
	float right_factor = alpha * (diff_q + efecto_grav);
	// calculo proximo u en i+1/2
	float u_next_iright = (1/_h_i_next) * (left_factor_base - right_factor); 
	return u_next_iright;
  }
  
  void showInfo(){
	cout<<"N-> "<<N<<endl;
	cout<<"Points-> "<<Points<<endl;
	cout<<"Topografia len "<<topografia.length()<<endl;
  }

  void newHeights(float* new_h){
	// line (altura) tiene Points elements
	int i_center = 0;
	int i_left = 0;
	int counter = 0;
	//showInfo();
	for (int i=0; i<N; i++){
	  i_center = getCenter(i);
	  i_left = getLeft(i);
	  // cout<<"pre h_imid"<<endl;
	  // showInfo();
	  // cout<<"i_left"<<i_left<<endl;

	  new_h[i_left] = h_imid(i_left - 1, i_left+1);
	  counter++;

	  // cout<<"pre h_i_next"<<endl;
	  // showInfo();
	  // cout<<"i_left"<<i_left<<endl;

	  new_h[i_center] =  h_i_next(i_center);
	  counter++;
	}
	new_h[Points-1] = h_imid(Points - 2, 0);
	counter++;
	cout<<"Counter end ->"<<counter<<endl;
  }

  void newVelocity(ShallowWaterUnidim& next_step, std::vector<float>& new_u){
	float value = 0.0;
	int i_right = 0;
	
	// in position index=1
	new_u.push_back(value);

	for (int i=0; i<N; i++){
	  // start inserting values in index: not {0,1} yes 2
	  // u_right
	  i_right = getRight(i);

	  // cout<<"pre u_i_next"<<endl;
	  // showInfo();
	  // cout<<"i_right "<<i_right<<endl;

	  value = u_i_next(i_right, next_step);
	  //cout<<"Calculando u-next value :: "<<value<<endl;
	  new_u.push_back(value);

	  //i_right = getRight(i);
	  if (i_right<Points-1){
		value = 0.0;
		new_u.push_back(value);
		} // if i_right == Points -1 not add 0.0
	}
	// close the tail
	// in position index=0
	float last_value = new_u[Points-1];
	//cout<<"Calculando u-next value :: "<<value<<endl;
	new_u.insert(new_u.begin(),last_value);

	//cout<<"New velocity ok :)-> size new_u "<<new_u.size()<<endl;
  }

  /* generar siguiente iteración */
  ShallowWaterUnidim next() {
	// generar nueva altura
	float* new_h =  new float[Points];	
	//cout<<"Calculando hnext en i"<<endl;
	newHeights(new_h);
    ShallowWaterUnidim next_step = ShallowWaterUnidim(new_h, *this);
	std::vector<float> new_u;
	//cout << "New velocity"<<endl;
	newVelocity(next_step, new_u);
	//cout<<"Asignando nuevo array de velocidades"<<endl;
	// u_i_next(int i,ShallowWaterUnidim& next_step)
	// set same curve before
	//cout<<"Set new curva"<<endl;
	CurvaGaussiana new_curva=CurvaGaussiana(&estimulo);
	new_curva.setCurva(new_u);
	next_step.setEstimulo(&new_curva);
	return next_step;
  }

  void showTopografia(bool is_base){
	if (is_base==true){
	  cout<<base<<endl;
	} else {
	  cout<<topografia<<endl;
	}
  }

  void showEstimulo() {
	  cout<<estimulo<<endl;
  }

  std::vector<float> getX(){
	return topografia.getX();
  }

  std::vector<float> getH(){
	std::vector<float> new_line;
	for (int i=0;i<Points;i++){
	  new_line.push_back(line[i]);
	}
	return new_line;
  }

  ShallowWaterUnidim& operator=(const ShallowWaterUnidim& other) {
	length = other.length;
	step = other.step;
	delta_time = other.delta_time;
	N = other.N;
	Points = other.Points;
	line = other.line;
	base = other.base;
	topografia = other.topografia;
	estimulo = other.estimulo;
	h_base = other.h_base;
	partial = false;
	return *this;
  }

};

int main(int argc, char *argv[]){
  float step = atof(argv[1]);
  float delta_time = atof(argv[2]);
  float amplitud = atof(argv[3]);
  fs::path path = argv[4];
  int iterations = atof(argv[5]);
  std::cout<<"Topografia "<<path<<std::endl;
  ShallowWaterUnidim sh0(step, delta_time, path, amplitud);
  std::cout<<sh0<<std::endl;
  sh0.showTopografia(true);
  sh0.showTopografia(false);
  sh0.showEstimulo();
  cout<<"END SH0"<<endl;

  float *nh = new float[sh0.getSize()];
  std::cout<<sh0<<std::endl;
  sh0.newHeights(nh);

  ShallowWaterUnidim next_step = ShallowWaterUnidim(nh, sh0);
  std::vector<float> new_u;
  sh0.newVelocity(next_step, new_u);

  auto start = std::chrono::high_resolution_clock::now();

  for (int i=0;i<iterations;i++){
	cout<<"END SH next i:"<<i<<endl;
	ShallowWaterUnidim sh1 = sh0.next();
	std::cout<<sh1<<std::endl;
	sh0 = sh1;
  }

  // Captura el tiempo de finalización
  auto end = std::chrono::high_resolution_clock::now();

  // Calcula el tiempo transcurrido en segundos
  std::chrono::duration<double> duration = end - start;
  double seconds = duration.count();

  // Imprime el tiempo transcurrido
  std::cout << "Tiempo transcurrido: " << seconds << " segundos" << std::endl;

  return 0;
};
