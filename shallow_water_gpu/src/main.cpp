#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>

#ifdef _WIN32
#include "../include/ShallowWater/plane.hpp"
#endif

#ifdef linux
#include "ShallowWater/plane.hpp"
#endif 

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>


namespace fs = std::filesystem;
using namespace glm;


std::string readFile(fs::path path)
{
    // Open the stream to 'lock' the file.
    std::ifstream f(path, std::ios::in | std::ios::binary);

    // Obtain the size of the file.
    const auto sz = fs::file_size(path);

    // Create a buffer.
    std::string result(sz, '\0');

    // Read the whole file into the buffer.
    f.read(result.data(), sz);

    return result;
}


class Shaders {
private:
  fs::path vertexShader_path;
  fs::path fragmentShader_path;
  std::string vertexShader;
  std::string fragmentShader;
public:
  Shaders(const std::string &vertex_path, const std::string &fragment_path) {
	this->vertexShader_path = std::filesystem::path("shaders/shaders") / vertex_path;
	this->fragmentShader_path = std::filesystem::path("shaders/shaders") / fragment_path;
	readShaders();
  }

  void readShaders(){
	this->vertexShader = readFile(this->vertexShader_path);
	this->fragmentShader = readFile(this->fragmentShader_path);
  }

  void show(){
	std::cout << this->vertexShader << std::endl;
	std::cout << this->fragmentShader << std::endl;
  }
  
  const std::string& fragment()  const{
	return this->fragmentShader;
  }

  const std::string& vertex() const{
	return this->vertexShader;
  }


  const std::string& fragment_path()  const{
	return this->fragmentShader_path;
  }

  const std::string& vertex_path() const{
	return this->vertexShader_path;
  }



};

void initGl() {
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 
}


// fn para cargar shader
GLuint cargarShader(GLenum tipoShader, const char* shaderSource)
{
    GLuint shader = glCreateShader(tipoShader);
    glShaderSource(shader, 1, &shaderSource, nullptr);
    glCompileShader(shader);

    // Verificar errores de compilación del shader
    GLint estadoCompilacion;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &estadoCompilacion);
    if (estadoCompilacion == GL_FALSE)
    {
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> logMensaje(logLength);
        glGetShaderInfoLog(shader, logLength, nullptr, logMensaje.data());
        std::cerr << "Error al compilar shader: " << logMensaje.data() << std::endl;
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}


/* end resize callback*/
int main(int argc, char *argv[]){
  std::cout <<"Shallow Water GPU"<<std::endl;
  Shaders shaders("vertex.glsl", "fragment.glsl");
  shaders.show();
  GLFWwindow* window;

  if( !glfwInit() )
  {
	  fprintf( stderr, "Failed to initialize GLFW\n" );
	  getchar();
	  return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);



  window = glfwCreateWindow( 1024, 768, "Shallow Water", NULL, NULL);
  if( window == NULL ){
	  fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
	  glfwTerminate();
	  return -1;
  }
  glfwMakeContextCurrent(window); // Initialize GLEW
  glewExperimental=true; // Needed in core profile
  if (glewInit() != GLEW_OK) {
	  fprintf(stderr, "Failed to initialize GLEW\n");
	  return -1;
  }

  // capture window

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  /*load shaders*/
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // Create and compile our GLSL program from the shaders
  const char *c_vertexShader = shaders.vertex().c_str();
  const char *c_fragmentShader = shaders.fragment().c_str();
  GLuint vertexShader = cargarShader(GL_VERTEX_SHADER, c_vertexShader);
  GLuint fragmentShader = cargarShader(GL_FRAGMENT_SHADER, c_fragmentShader);
  

  // en esto se toma la iteración, es un Guint
   GLuint shaderProgram = glCreateProgram();
   // Adjunta los shaders al programa
   glAttachShader(shaderProgram, vertexShader);
   glAttachShader(shaderProgram, fragmentShader);

   // Enlaza los shaders
   glLinkProgram(shaderProgram);

	// Enlaza los shaders
	glLinkProgram(shaderProgram);

	// Verifica errores de enlace
	GLint estadoEnlace;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &estadoEnlace);
	if (estadoEnlace == GL_FALSE)
	{
		GLint logLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> logMensaje(logLength);
		glGetProgramInfoLog(shaderProgram, logLength, nullptr, logMensaje.data());
		std::cerr << "Error al enlazar el programa de shader: " << logMensaje.data() << std::endl;
	}

	// Valida el programa
	glValidateProgram(shaderProgram);

	// Utiliza el programa en tu código OpenGL


	static const GLfloat g_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 0.0f,  1.0f, 0.0f,
	};

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);


  /*end load shaders*/
  do{
	  // Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
	  glClear( GL_COLOR_BUFFER_BIT );
	  glUseProgram(shaderProgram);

	  // Draw nothing, see you in tutorial 2 !
	  // 1rst attribute buffer : vertices
	  glEnableVertexAttribArray(0);
	  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	  glVertexAttribPointer(
		  0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		  3,                  // size
		  GL_FLOAT,           // type
		  GL_FALSE,           // normalized?
		  0,                  // stride
		  (void*)0            // array buffer offset
	  );

	  // Draw the triangle !
	  glDrawArrays(GL_TRIANGLES, 0, 3); // 3 indices starting at 0 -> 1 triangle

	  glDisableVertexAttribArray(0);

	  // Swap buffers
	  glfwSwapBuffers(window);
	  glfwPollEvents();

  } // Check if the ESC key was pressed or the window was closed
  while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		 glfwWindowShouldClose(window) == 0 );


  return 0;
}
