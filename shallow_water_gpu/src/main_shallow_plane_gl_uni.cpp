#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo
#include "ShallowWater/topografia_uni.hpp"
#include "ShallowWater/curva_gaussiana_uni.hpp"
#include "ShallowWater/unidimensional.hpp"
#include "ShallowWater/mesh_gl_uni.hpp"
#include "ShallowWater/shaders.hpp"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace fs = std::filesystem;
using namespace glm;


void initGl() {
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 
}


// fn para cargar shader
GLuint cargarShader(GLenum tipoShader, const char* shaderSource)
{
    GLuint shader = glCreateShader(tipoShader);
    glShaderSource(shader, 1, &shaderSource, nullptr);
    glCompileShader(shader);

    // Verificar errores de compilación del shader
    GLint estadoCompilacion;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &estadoCompilacion);
    if (estadoCompilacion == GL_FALSE)
    {
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> logMensaje(logLength);
        glGetShaderInfoLog(shader, logLength, nullptr, logMensaje.data());
        std::cerr << "Error al compilar shader: " << logMensaje.data() << std::endl;
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}




/* end resize callback*/
int main(int argc, char *argv[]){
  std::cout <<"Shallow Water GPU"<<std::endl;
  Shaders shaders("mesh_gl/vertex.glsl", "mesh_gl/fragment.glsl");
  shaders.show();
  GLFWwindow* window;

  if( !glfwInit() )
  {
	  fprintf( stderr, "Failed to initialize GLFW\n" );
	  getchar();
	  return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);



  window = glfwCreateWindow( 1024, 768, "Triangle with matrixes", NULL, NULL);
  if( window == NULL ){
	  fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
	  getchar();
	  glfwTerminate();
	  return -1;
  }
  glfwMakeContextCurrent(window); // Initialize GLEW
  glewExperimental=true; // Needed in core profile
  if (glewInit() != GLEW_OK) {
	  fprintf(stderr, "Failed to initialize GLEW\n");
	  return -1;
  }

  // capture window

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  /*load shaders*/
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // Create and compile our GLSL program from the shaders
  const char *c_vertexShader = shaders.vertex().c_str();
  const char *c_fragmentShader = shaders.fragment().c_str();
  GLuint vertexShader = cargarShader(GL_VERTEX_SHADER, c_vertexShader);
  GLuint fragmentShader = cargarShader(GL_FRAGMENT_SHADER, c_fragmentShader);
  

  // en esto se toma la iteración, es un Guint
   GLuint shaderProgram = glCreateProgram();
   // Adjunta los shaders al programa
   glAttachShader(shaderProgram, vertexShader);
   glAttachShader(shaderProgram, fragmentShader);

   // Enlaza los shaders
   glLinkProgram(shaderProgram);

	// Enlaza los shaders
	glLinkProgram(shaderProgram);

	// Verifica errores de enlace
	GLint estadoEnlace;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &estadoEnlace);
	if (estadoEnlace == GL_FALSE)
	{
		GLint logLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> logMensaje(logLength);
		glGetProgramInfoLog(shaderProgram, logLength, nullptr, logMensaje.data());
		std::cerr << "Error al enlazar el programa de shader: " << logMensaje.data() << std::endl;
	}

	// Valida el programa
	glValidateProgram(shaderProgram);

	// Utiliza el programa en tu código OpenGL
	// creamos las variables  y matrices a usar:
	Mesh mesh(1.0f, 4.0f, 0.1f); // L = M = 1.0, step = 0.1

	// se obtiene el id de la variable uniform en vertex
	GLuint MatrixID = glGetUniformLocation(shaderProgram, "MVP");
	// se define una matriz de proyección:

	glm::mat4 Projection = glm::perspective(glm::radians(45.0f),4.0f/3.00f,0.1f,100.0f);

	// camara matrix
	glm::mat4 View = glm::lookAt(
								 glm::vec3(4,3,3),
								 glm::vec3(0,0,0),
								 glm::vec3(0,1,0)
							  );
	// Model matrix :: identity matrix
	glm::mat4 Model = glm::mat4(1.0f);
	
	// modelviewprojection :: multiplicacion de las 3 matrices

	glm::mat4 MVP = Projection * View * Model;

	// ahora lo mismo, definir los vértices del triangulo

	static const GLfloat g_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 0.0f,  1.0f, 0.0f,
	};

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);


  /*end load shaders*/
  do{
	  // Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
	  glClear( GL_COLOR_BUFFER_BIT );
	  glUseProgram(shaderProgram);

	  // cargar la proyección

	  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	  // // Draw nothing, see you in tutorial 2 !
	  // // 1rst attribute buffer : vertices
	  // glEnableVertexAttribArray(0);
	  // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	  // glVertexAttribPointer(
	  // 	  0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
	  // 	  3,                  // size
	  // 	  GL_FLOAT,           // type
	  // 	  GL_FALSE,           // normalized?
	  // 	  0,                  // stride
	  // 	  (void*)0            // array buffer offset
	  // );

	  // // Draw the triangle !
	  // glDrawArrays(GL_TRIANGLES, 0, 3); // 3 indices starting at 0 -> 1 triangle

	  // glDisableVertexAttribArray(0);


	  mesh.Draw();

	  // Swap buffers
	  glfwSwapBuffers(window);
	  glfwPollEvents();

  } // Check if the ESC key was pressed or the window was closed
  while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		 glfwWindowShouldClose(window) == 0 );


  // Cleanup VBO and shader
  glDeleteBuffers(1, &vertexbuffer);
  glDeleteProgram(shaderProgram);
  glDeleteVertexArrays(1, &VertexArrayID);

  // Close OpenGL window and terminate GLFW
  glfwTerminate();

  return 0;
}
