#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <vector>
#include <tuple>
#include "ShallowWater/plane.hpp"

MeshPlane::MeshPlane(std::vector<std::vector<double>> init_cond, int x, int y, double distance) {
        this->x = x;
		this->y = y;
		this->distance = distance;
		this->H = init_cond;
		this->current_h = 0;
        applyCurrentH();
        initializeStuff();
    }

int MeshPlane::getCurrentH() const {
        return current_h;
    }

void MeshPlane::setCurrentH(int a) {
        current_h = a;
    }

std::vector<std::vector<double>> MeshPlane::getH() const {
        return H;
    }

void MeshPlane::setH(std::vector<std::vector<double>> a) {
        H = a;
    }

int MeshPlane::getX() const {
        return x;
    }

void MeshPlane::setX(int a) {
        x = a;
    }

int MeshPlane::getY() const {
        return y;
    }

void MeshPlane::setY(int a) {
        y = a;
    }

double MeshPlane::getDistance() const {
        return distance;
    }

void MeshPlane::setDistance(double a) {
        distance = a;
    }

void MeshPlane::nextStep() {
        if (H[0].size() - 1 <= current_h + 1) {
            current_h = 0;
        } else {
            current_h += 1;
        }
        applyCurrentH();
    }

void MeshPlane::applyCurrentH() {
        std::vector<double> current_h_values;
        for (const auto& row : H) {
            current_h_values.push_back(row[current_h]);
        }
        double start_y = -((y / 2) * distance);
        vertices.clear();
        for (int j = 0; j < y; j++) {
            double start_x = -((x / 2) * distance);
            for (int i = 0; i < x; i++) {
                vertices.push_back({start_x, current_h_values[j], start_y});
                start_x += distance;
            }
            start_y += distance;
        }
    }

void MeshPlane::initializeStuff() {
        double start_y = -((y / 2) * distance);
        vertices.clear();
        for (int j = 0; j < y; j++) {
            double start_x = -((x / 2) * distance);
            for (int i = 0; i < x; i++) {
                vertices.push_back({start_x, 0, start_y});
                start_x += distance;
            }
            start_y += distance;
        }

        edges.clear();
        for (int j = 0; j < y; j++) {
            if (j == y - 1) {
                for (int i = 0; i < x - 1; i++) {
                    edges.push_back({i + (j * x), i + (j * x) + 1});
                }
                break;
            }
            for (int i = 0; i < x - 1; i++) {
                edges.push_back({i + (j * x), i + (j * x) + 1});
            }
            for (int i = 0; i < x; i++) {
                if (i == 0) {
                    edges.push_back({j * x, ((j + 1) * x)});
                } else {
                    edges.push_back({i + (j * x), (i - 1) + ((j + 1) * x)});
                    edges.push_back({i + (j * x), i + ((j + 1) * x)});
                }
            }
        }

        surfaces.clear();
        for (int j = 0; j < y - 1; j++) {
            for (int i = 0; i < x - 1; i++) {
                surfaces.push_back(std::make_tuple(i + (j * x), i + 1 + (j * x), i + x + (j * x)));
                surfaces.push_back(std::make_tuple(i + 1 + (j * x), i + x + (j * x), i + x + 1 + (j * x)));
            }
        }

        colors.clear();
        for (size_t i = 0; i < edges.size(); i++) {
            if (i % 2 == 0) {
                colors.push_back(std::make_tuple(0.3f, 0.3f, 1.0f));
            } else {
                colors.push_back(std::make_tuple(0.6f, 1.0f, 1.0f));
            }
        }
    }

void MeshPlane::updatePlane() {
        glBegin(GL_TRIANGLES);
        size_t x = 0;
        for (const auto& surface : surfaces) {
            x += 1;
            size_t i = 0;
            for (const auto& vertex : std::vector<int>{std::get<0>(surface), std::get<1>(surface), std::get<2>(surface)}) {
                i += 1;
                const GLfloat* color = reinterpret_cast<const GLfloat*>(&std::get<0>(colors[x])) + i - 1;
                glColor3fv(color);
                glVertex3dv(reinterpret_cast<const GLdouble*>(&vertices[vertex]));
            }
        }
        glEnd();

        glBegin(GL_LINES);
        for (const auto& edge : edges) {
            const auto& vertex1 = edge.first;
            const auto& vertex2 = edge.second;
            glVertex3dv(reinterpret_cast<const GLdouble*>(&vertices[vertex1]));
            glVertex3dv(reinterpret_cast<const GLdouble*>(&vertices[vertex2]));
        }
        glEnd();
    }

// MeshPlane plane(std::vector<std::vector<double>>(), 10, 10, 1); // Se debe proporcionar una matriz de condiciones iniciales adecuada

// void display() {
//     glClearColor(0.0, 0.0, 0.0, 1.0);
//     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//     glMatrixMode(GL_MODELVIEW);
//     glLoadIdentity();

//     gluLookAt(0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

//     plane.updatePlane();

//     glutSwapBuffers();
// }

// void reshape(int width, int height) {
//     glViewport(0, 0, width, height);

//     glMatrixMode(GL_PROJECTION);
//     glLoadIdentity();

//     gluPerspective(45.0, static_cast<double>(width) / static_cast<double>(height), 0.1, 100.0);
// }

// void timer(int value) {
//     plane.nextStep();
//     glutPostRedisplay();
//     glutTimerFunc(100, timer, 0);
// }

// int main(int argc, char** argv) {
//     glutInit(&argc, argv);
//     glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
//     glutInitWindowSize(800, 600);
//     glutCreateWindow("Mesh Plane");

//     glutDisplayFunc(display);
//     glutReshapeFunc(reshape);
//     glutTimerFunc(0, timer, 0);

//     glEnable(GL_DEPTH_TEST);

//     glutMainLoop();

//     return 0;
// }
