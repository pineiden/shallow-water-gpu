#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include "ShallowWater/curva_gaussiana_uni.hpp"

namespace fs = std::filesystem;
using namespace std;


    // Constructor que inicializa el array de N floats como una curva gaussiana con centro en i, desviación estándar de j celdas y amplitud de onda
CurvaGaussiana::CurvaGaussiana(int size, int i, int j, float amplitud_onda) : amplitud(amplitud_onda) {
        N = size;

        // Calcular el centro de la curva gaussiana
        float centro = i;

        // Calcular la desviación estándar
        float desviacion = j;

        // Calcular la constante de normalización para la gaussiana
        float normalizacion = amplitud / (desviacion * std::sqrt(2.0f * M_PI));


        // Llenar el array con los valores de la curva gaussiana
        for (int k = 0; k < N; k++) {
            float distanciaAlCentro = k - centro;
            float val = normalizacion * std::exp(-0.5f * std::pow(distanciaAlCentro / desviacion, 2));
			datos.push_back(val);
        }
    }

CurvaGaussiana::CurvaGaussiana(CurvaGaussiana* curva,std::vector<float>& new_datos){
	N = curva->N;
	amplitud = curva->amplitud;	

	for (int i = 0; i <new_datos.size();i++){
	  float value = new_datos[i];
	  datos.push_back(new_datos[i]);
	}

  }


void CurvaGaussiana::setCurva(std::vector<float>& new_datos){
  
  for (int i = 0; i <new_datos.size();i++){
	float value = new_datos[i];
	datos.push_back(new_datos[i]);
	  }
	}
 
    // Destructor para liberar la memoria del array
CurvaGaussiana::CurvaGaussiana() {
    }

    // Destructor para liberar la memoria del array
CurvaGaussiana::~CurvaGaussiana() {
        //delete[] datos;
    }

int CurvaGaussiana::getSize() {
        //delete[] datos;
  return datos.size();
}



    // Obtener el valor en una posición del array
float CurvaGaussiana::getValor(int index) const {
        if (index >= 0 && index < N) {
            return datos[index];
        } else {
            throw std::out_of_range("Índice fuera del rango del array.");
        }
    }


std::ostream& operator<<(
						   std::ostream& os, 
						   const CurvaGaussiana& v) {

	os << "Curva Gaussiana x,y"<<endl;
	for (unsigned int index=0; index < v.N; index++){
	  os <<  index << "," << v.datos[index]<<endl;
	}
	return os;
  }


CurvaGaussiana& CurvaGaussiana::operator=(const CurvaGaussiana& other) {
	// Verifica si no se está asignando a sí mismo
	if (this != &other) {
		// Realiza la asignación de los miembros
		N = other.N;
		amplitud = other.amplitud;
		datos = other.datos;
	}
	// Devuelve el propio objeto para permitir asignaciones encadenadas
	return *this;
}


float CurvaGaussiana::maxZ() const {
	  if (datos.empty()) {
		  // Si el vector está vacío, devuelve un valor predeterminado o lanza una excepción si es apropiado.
		  // Aquí devolveré un valor predeterminado de 0, pero puedes adaptarlo según tus necesidades.
		  return 0.0;
	  }

	  // Usamos la función std::max_element de la biblioteca algorithm para encontrar el valor máximo en el vector.
	  auto maxElement = std::max_element(datos.begin(), datos.end());

	  // Devolvemos el valor máximo encontrado
	  return *maxElement;
  }


