#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <thread>

#ifdef _WIN32
#include "../include/ShallowWater/plane.hpp"
#endif

#ifdef linux
#include "ShallowWater/plane.hpp"

#endif 

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "ShallowWater/mesh_gl_uni.hpp"
#include "ShallowWater/shaders.hpp"
#include "ShallowWater/topografia_uni.hpp"
#include "ShallowWater/curva_gaussiana_uni.hpp"
#include "ShallowWater/unidimensional.hpp"
#include "ShallowWater/mesh_gl_uni.hpp"



namespace fs = std::filesystem;
using namespace glm;


void guardarVectorEnCSV(
const std::vector<float>& vector, 
const std::string& nombreArchivo) {
    std::ofstream archivo(nombreArchivo, std::ios::app); // Abrir el archivo en modo de adjuntar datos

    if (archivo.is_open()) {
        for (size_t i = 0; i < vector.size(); ++i) {
            archivo << vector[i]; // Escribir el valor del vector
            if (i < vector.size() - 1) {
                archivo << ","; // Agregar una coma después del valor, excepto para el último valor de la fila
            }
        }
        archivo << "\n"; // Agregar un salto de línea al final de la fila
        archivo.close(); // Cerrar el archivo
        std::cout << "Vector guardado en el archivo: " << nombreArchivo << std::endl;
    } else {
        std::cerr << "No se pudo abrir el archivo: " << nombreArchivo << std::endl;
    }
}


void initGl() {
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 
}


// fn para cargar shader
GLuint cargarShader(GLenum tipoShader, const char* shaderSource)
{
    GLuint shader = glCreateShader(tipoShader);
    glShaderSource(shader, 1, &shaderSource, nullptr);
    glCompileShader(shader);

    // Verificar errores de compilación del shader
    GLint estadoCompilacion;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &estadoCompilacion);
    if (estadoCompilacion == GL_FALSE)
    {
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        std::vector<GLchar> logMensaje(logLength);
        glGetShaderInfoLog(shader, logLength, nullptr, logMensaje.data());
        std::cerr << "Error al compilar shader: " << logMensaje.data() << std::endl;
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}



std::vector<float> replicateHeight(
	std::vector<float>h, 
	unsigned int W){
  std::vector<float> new_h;
  float val;
  for (int i = 0; i < h.size(); i++){
	val = h[i];
	for (int j=0;j<W;j++){
	  new_h.push_back(val);
	}
  }
  return new_h;  
}



#include <cmath>


std::vector<float> sinMesh(
	unsigned int L, 
	unsigned int W,
	float A, int counter
){
  std::vector<float> new_h;
  float val;
  for (int i = 0; i < L; i++){

	val = A*sin((((float)i)/(100*3.14))*((float)counter));
	for (int j=0;j<W;j++){
	  new_h.push_back(val);
	}

  }
  return new_h;  
}


bool mouseLeftPressed = false;
double lastMouseX = 0.0;
double lastMouseY = 0.0;

float fov = 45.0f;
glm::mat4 View = glm::lookAt(glm::vec3(4,5,3), glm::vec3(0,0,0), glm::vec3(0,1,0));


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    fov -= (float)yoffset;
    if (fov < 1.0f)
        fov = 1.0f;
    if (fov > 45.0f)
        fov = 45.0f; 
}


void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            mouseLeftPressed = true;
            glfwGetCursorPos(window, &lastMouseX, &lastMouseY);
        } else if (action == GLFW_RELEASE) {
            mouseLeftPressed = false;
        }
    }
}

void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    if (mouseLeftPressed) {
        double deltaX = xpos - lastMouseX;
        double deltaY = ypos - lastMouseY;

        // Adjust the view matrix to move the camera based on mouse movement
        // You can adjust these values to control the sensitivity of the mouse movement
        float rotationSpeed = 0.1f;
        View = glm::rotate(View, static_cast<float>(deltaX) * rotationSpeed, glm::vec3(0, 1, 0));
        View = glm::rotate(View, static_cast<float>(deltaY) * rotationSpeed, glm::vec3(1, 0, 0));

        lastMouseX = xpos;
        lastMouseY = ypos;
    }
}



/* end resize callback*/
int main(int argc, char *argv[]){
  //test();
  // largo vendrá dado desde la topografía lineal

  float step = atof(argv[1]);
  float delta_time = atof(argv[2]);
  float amplitud = atof(argv[3]);
  fs::path path = argv[4];
  int iterations = atof(argv[5]);
  float h_base = atof(argv[6]);
  const float ancho = atof(argv[7]);

  ShallowWaterUnidim sh0(step, delta_time, path, amplitud, h_base);
  std::cout<<sh0<<std::endl;
 
  // ancho 120.0
  float largo = sh0.getLength();
  float N = sh0.nCells();
  std::vector<float> heights = sh0.getH();
  std::vector<float> positions = sh0.getX();
  guardarVectorEnCSV(positions, "positions.txt");

  cout << "Length "<<largo<<endl;
  cout << "Width "<<ancho<<endl;
  cout << "Celdas "<<N<<endl;
  cout << "Cells in Heights "<<heights.size()<<endl;


  std::cout <<"Shallow Water GPU"<<std::endl;
  Shaders shaders("mesh_gl/vertex.glsl", "mesh_gl/fragment.glsl");
  shaders.show();
  GLFWwindow* window;

  if( !glfwInit() )
  {
	  fprintf( stderr, "Failed to initialize GLFW\n" );
	  getchar();
	  return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);



  window = glfwCreateWindow( 1024, 768, "Triangle with matrixes", NULL, NULL);
  if( window == NULL ){
	  fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
	  getchar();
	  glfwTerminate();
	  return -1;
  }
  glfwMakeContextCurrent(window); // Initialize GLEW
  glewExperimental=true; // Needed in core profile
  if (glewInit() != GLEW_OK) {
	  fprintf(stderr, "Failed to initialize GLEW\n");
	  return -1;
  }

  // capture window

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  /*load shaders*/
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // Create and compile our GLSL program from the shaders
  const char *c_vertexShader = shaders.vertex().c_str();
  const char *c_fragmentShader = shaders.fragment().c_str();
  GLuint vertexShader = cargarShader(GL_VERTEX_SHADER, c_vertexShader);
  GLuint fragmentShader = cargarShader(GL_FRAGMENT_SHADER, c_fragmentShader);
  
  // en esto se toma la iteración, es un Guint
   GLuint shaderProgram = glCreateProgram();
   // Adjunta los shaders al programa
   glAttachShader(shaderProgram, vertexShader);
   glAttachShader(shaderProgram, fragmentShader);

   // Enlaza los shaders
   glLinkProgram(shaderProgram);

	// Enlaza los shaders
	glLinkProgram(shaderProgram);

	// Verifica errores de enlace
	GLint estadoEnlace;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &estadoEnlace);
	if (estadoEnlace == GL_FALSE)
	{
		GLint logLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> logMensaje(logLength);
		glGetProgramInfoLog(shaderProgram, logLength, nullptr, logMensaje.data());
		std::cerr << "Error al enlazar el programa de shader: " << logMensaje.data() << std::endl;
	}

	// Valida el programa
	glValidateProgram(shaderProgram);

	// Utiliza el programa en tu código OpenGL
	// creamos las variables  y matrices a usar:

	// se obtiene el id de la variable uniform en vertex
	GLuint MatrixID = glGetUniformLocation(shaderProgram, "MVP");
	// se define una matriz de proyección:
	Mesh mesh(largo, ancho, step); 
	// L = M = 1.0, N: nro celdas, step = 0.1
	mesh.initializeBuffers();
    mesh.setupVertexAttribs(shaderProgram);

    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetScrollCallback(window, scroll_callback);


	glm::mat4 Projection = glm::perspective(glm::radians(45.0f),4.0f/3.00f,0.1f,100.0f);

	// camara matrix
	glm::mat4 View = glm::lookAt(
								 glm::vec3(4,3,3),
								 glm::vec3(0,0,0),
								 glm::vec3(0,1,0)
							  );
	// Model matrix :: identity matrix
	glm::mat4 Model = glm::mat4(1.0f);
	
	// modelviewprojection :: multiplicacion de las 3 matrices

	glm::mat4 MVP = Projection * View * Model;

	// ahora lo mismo, definir los vértices del triangulo

	static const GLfloat g_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 0.0f,  1.0f, 0.0f,
	};

	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);


	/*
	  guardar alturas
	 */
    std::ofstream archivo("alturas.txt", std::ios::app); // Abrir el archivo en modo de adjuntar datos

  int counter=0;
  /*end load shaders*/
  /* lectura de coordenadas o generación de ma*/



  /**/
  do{
	  // Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
	  glClear( GL_COLOR_BUFFER_BIT );
	  glUseProgram(shaderProgram);

	  // cargar la proyección
      glm::mat4 Projection = glm::perspective(glm::radians(fov), 4.0f / 3.0f, 0.1f, 100.0f);
      glm::mat4 MVP = Projection * View * Model;

	  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	  // Draw nothing, see you in tutorial 2 !
	  // 1rst attribute buffer : vertices
	  // glEnableVertexAttribArray(0);
	  // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	  // glVertexAttribPointer(
	  // 	  0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
	  // 	  3,                  // size
	  // 	  GL_FLOAT,           // type
	  // 	  GL_FALSE,           // normalized?
	  // 	  0,                  // stride
	  // 	  (void*)0            // array buffer offset
	  // );

	  // // Draw the triangle !
	  // glDrawArrays(GL_TRIANGLES, 0, 3); // 3 indices starting at 0 -> 1 triangle

	  // glDisableVertexAttribArray(0);
	  heights = sh0.getH();
	  // update heights to mesh

	  //std::vector<float> mesh_heights = sinMesh(largo,ancho,12.5, counter);
	  std::vector<float> mesh_heights = replicateHeight(
	    heights, 
		ancho);

    if (archivo.is_open()) {
        for (size_t i = 0; i < mesh_heights.size(); ++i) {
            archivo << mesh_heights[i]; // Escribir el valor del vector
            if (i < mesh_heights.size() - 1) {
                archivo << ","; // Agregar una coma después del valor, excepto para el último valor de la fila
            }
        }
        archivo << "\n"; // Agregar un salto de línea al final de la fila
    } else {
        std::cerr << "No se pudo abrir el archivo: " << "alturas.txt" << std::endl;
    }

	  mesh.updateY(mesh_heights);
	  
	  float maxHeight = 2.5f; // Calcula la altura máxima actual de la malla
	  float minHeight = 1.8f; // Calcula la altura mínima actual de la malla

	  // Send the updated maxHeight and minHeight to the vertex shader
	  GLuint maxHeightID = glGetUniformLocation(shaderProgram, "maxHeight");
	  glUniform1f(maxHeightID, maxHeight);

	  GLuint minHeightID = glGetUniformLocation(shaderProgram, "minHeight");
	  glUniform1f(minHeightID, minHeight);

	  // draw new height to opengl
	  mesh.draw(shaderProgram);
	  // Swap buffers


	  // refresh gl
	  glfwSwapBuffers(window);
	  glfwPollEvents();

	  // recalculate shallow_water iteration
	  ShallowWaterUnidim sh1 = sh0.next();
	  sh0 = sh1;

	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	  counter++;
	  cout<<"Iteration counter"<<counter<<std::endl;

  } // Check if the ESC key was pressed or the window was closed
  while (( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		 glfwWindowShouldClose(window) == 0 ) && counter<iterations);

  cout<<"End gl"<<endl;
  archivo.close(); // Cerrar el archivo

  // Cleanup VBO and shader
  glDeleteBuffers(1, &vertexbuffer);
  glDeleteProgram(shaderProgram);
  glDeleteVertexArrays(1, &VertexArrayID);

  // Close OpenGL window and terminate GLFW
  glfwTerminate();

  return 0;
}
