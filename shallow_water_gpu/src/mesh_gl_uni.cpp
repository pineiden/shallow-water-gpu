#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// project libs
#include "ShallowWater/mesh_gl_uni.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

Mesh::Mesh(int L, int M, float step) : L_(L), M_(M), step_(step) {


        numVertices_ = L_ * M_;
        numIndices_ = (L_ - 1) * (M_ - 1) * 6;

        // Generate vertices
        vertices_.resize(numVertices_);
        for (int i = 0; i < L_; ++i) {
		    float z = step_ * i;
            for (int j = 0; j < M_; ++j) {
                float x = step_ * j;
                float y = 0.0f;
                vertices_[i * M_ + j] = vec3(x, y, z);
            }
        }

        // Generate indices
        indices_.resize(numIndices_);
        int index = 0;
        for (int i = 0; i < L_ - 1; ++i) {
            for (int j = 0; j < M_ - 1; ++j) {
                int topLeft = i * M_ + j;
                int topRight = topLeft + 1;
                int bottomLeft = (i + 1) * M_ + j;
                int bottomRight = bottomLeft + 1;

                indices_[index++] = topLeft;
                indices_[index++] = bottomLeft;
                indices_[index++] = topRight;

                indices_[index++] = topRight;
                indices_[index++] = bottomLeft;
                indices_[index++] = bottomRight;
            }
        }

        // Generate color buffer
        colors_.resize(numVertices_, vec3(0.0f, 0.0f, 1.0f));

    }



void Mesh::initializeBuffers() {
	// Generate vertex array object
	glGenVertexArrays(1, &vertexArrayID_);
	glBindVertexArray(vertexArrayID_);

	// Generate vertex buffer
	glGenBuffers(1, &vertexbuffer_);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
	glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &vertices_[0], GL_STATIC_DRAW);

	// Generate color buffer
	glGenBuffers(1, &colorbuffer_);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer_);
	glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &colors_[0], GL_STATIC_DRAW);

	// Generate element buffer
	glGenBuffers(1, &elementbuffer_);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices_ * sizeof(unsigned int), &indices_[0], GL_STATIC_DRAW);
}


void Mesh::setupVertexAttribs(GLuint programID) {
	// Bind vertex array object
	glBindVertexArray(vertexArrayID_);

	// Bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Bind color buffer
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer_);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
}


Mesh::~Mesh() {
        glDeleteBuffers(1, &vertexbuffer_);
        glDeleteBuffers(1, &colorbuffer_);
        glDeleteBuffers(1, &elementbuffer_);
        glDeleteVertexArrays(1, &vertexArrayID_);
    }

void Mesh::draw(GLuint programID) {
        // Bind vertex array object
        glBindVertexArray(vertexArrayID_);
        // Use our shader
        glUseProgram(programID);
        // Draw the mesh
        glDrawElements(GL_TRIANGLES, numIndices_, GL_UNSIGNED_INT, 0);
    }



void Mesh::updateY(std::vector<float> vertexValues) {
	 for (int i = 0; i < L_; ++i) {
		 for (int j = 0; j < M_; ++j) {
			 // Calcular el índice correspondiente en el vector de valores
			 int valueIndex = j % vertexValues.size();
			 // Actualizar el valor de altura para el vértice actual
			 vertices_[i * M_ + j].y = vertexValues[valueIndex];
		 }
	 }


	// Actualizar el búfer de vértices
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
	glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &vertices_[0], GL_STATIC_DRAW);
}
