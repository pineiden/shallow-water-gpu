#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <chrono>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
GLFWwindow* window;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include <common/shader.hpp>

class Mesh {
public:
    Mesh(int L, int M, float step) : L_(L), M_(M), step_(step) {
        numVertices_ = L_ * M_;
        numIndices_ = (L_ - 1) * (M_ - 1) * 6;

        // Generate vertices
        vertices_.resize(numVertices_);
        for (int i = 0; i < L_; ++i) {
            for (int j = 0; j < M_; ++j) {
                float x = step_ * j;
                float y = 0.0f;
                float z = step_ * i;
                vertices_[i * M_ + j] = vec3(x, y, z);
            }
        }

        // Generate indices
        indices_.resize(numIndices_);
        int index = 0;
        for (int i = 0; i < L_ - 1; ++i) {
            for (int j = 0; j < M_ - 1; ++j) {
                int topLeft = i * M_ + j;
                int topRight = topLeft + 1;
                int bottomLeft = (i + 1) * M_ + j;
                int bottomRight = bottomLeft + 1;

                indices_[index++] = topLeft;
                indices_[index++] = bottomLeft;
                indices_[index++] = topRight;

                indices_[index++] = topRight;
                indices_[index++] = bottomLeft;
                indices_[index++] = bottomRight;
            }
        }

        // Generate color buffer
        colors_.resize(numVertices_, vec3(0.0f, 0.0f, 1.0f));
    }

    void updateYFromTextFile(std::vector<float> vertexValues) {
        
		for (int i=0; i<(M_*L_); i++){
			vertices_[i].y = vertexValues[i];
		}
        
        // Actualizar el búfer de vértices
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
        glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &vertices_[0], GL_STATIC_DRAW);
    }

    void draw(GLuint programID) {
        // Bind vertex array object
        glBindVertexArray(vertexArrayID_);

        // Use our shader
        glUseProgram(programID);

        // Draw the mesh
        glDrawElements(GL_TRIANGLES, numIndices_, GL_UNSIGNED_INT, 0);
    }

private:
    int L_;                  // Largo de la malla (número de filas)
    int M_;                  // Ancho de la malla (número de columnas)
    float step_;             // Separación entre vértices
    int numVertices_;        // Número total de vértices
    int numIndices_;         // Número total de índices
    std::vector<vec3> vertices_;    // Vector de vértices
    std::vector<vec3> colors_;      // Vector de colores
    std::vector<unsigned int> indices_; // Vector de índices

    GLuint vertexArrayID_;   // ID del Vertex Array Object (VAO)
    GLuint vertexbuffer_;    // ID del buffer de vértices
    GLuint colorbuffer_;     // ID del buffer de colores
    GLuint elementbuffer_;   // ID del buffer de índices

public:
    void initializeBuffers() {
        // Generate vertex array object
        glGenVertexArrays(1, &vertexArrayID_);
        glBindVertexArray(vertexArrayID_);

        // Generate vertex buffer
        glGenBuffers(1, &vertexbuffer_);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
        glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &vertices_[0], GL_STATIC_DRAW);

        // Generate color buffer
        glGenBuffers(1, &colorbuffer_);
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer_);
        glBufferData(GL_ARRAY_BUFFER, numVertices_ * sizeof(vec3), &colors_[0], GL_STATIC_DRAW);

        // Generate element buffer
        glGenBuffers(1, &elementbuffer_);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer_);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices_ * sizeof(unsigned int), &indices_[0], GL_STATIC_DRAW);
    }

    void setupVertexAttribs(GLuint programID) {
        // Bind vertex array object
        glBindVertexArray(vertexArrayID_);

        // Bind vertex buffer
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        // Bind color buffer
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer_);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    }
};

bool mouseLeftPressed = false;
double lastMouseX = 0.0;
double lastMouseY = 0.0;

float fov = 45.0f;
glm::mat4 View = glm::lookAt(glm::vec3(4,5,3), glm::vec3(0,0,0), glm::vec3(0,1,0));


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    fov -= (float)yoffset;
    if (fov < 1.0f)
        fov = 1.0f;
    if (fov > 45.0f)
        fov = 45.0f; 
}


void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            mouseLeftPressed = true;
            glfwGetCursorPos(window, &lastMouseX, &lastMouseY);
        } else if (action == GLFW_RELEASE) {
            mouseLeftPressed = false;
        }
    }
}

void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    if (mouseLeftPressed) {
        double deltaX = xpos - lastMouseX;
        double deltaY = ypos - lastMouseY;

        // Adjust the view matrix to move the camera based on mouse movement
        // You can adjust these values to control the sensitivity of the mouse movement
        float rotationSpeed = 0.1f;
        View = glm::rotate(View, static_cast<float>(deltaX) * rotationSpeed, glm::vec3(0, 1, 0));
        View = glm::rotate(View, static_cast<float>(deltaY) * rotationSpeed, glm::vec3(1, 0, 0));

        lastMouseX = xpos;
        lastMouseY = ypos;
    }
}



int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Tutorial 04 - Colored Cube", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	int L = 8;  // Largo de la malla
    int M = 5;  // Ancho de la malla
    float step = 0.3f;  // Separación entre vértices
    Mesh mesh(L, M, step);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "./TransformVertexShader.vertexshader", "./ColorFragmentShader.fragmentshader" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	mesh.initializeBuffers();
    mesh.setupVertexAttribs(programID);

    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetScrollCallback(window, scroll_callback);

	glm::mat4 Model = glm::mat4(1.0f);

	std::string filename = "coordenadas.txt";
	std::ifstream file(filename);
	if (!file.is_open()) {
            fprintf(stderr, "Failed to open file: %s\n", filename.c_str());
            return 0;
        }

    std::string line;
    std::vector<std::vector<float>> vertexValues;  // Almacenar los valores de los vértices

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<float> rowValues;
        float value;

        while (iss >> value) {
            rowValues.push_back(value);
        }

        if (!rowValues.empty()) {
            vertexValues.push_back(rowValues);
        }
    }

    std::vector<std::vector<float>> columnValues;
    size_t num_rows = vertexValues.size();
    size_t num_cols = vertexValues[0].size();

    columnValues.resize(num_cols, std::vector<float>(num_rows));
    for (size_t i = 0; i < num_rows; i++) {
        for (size_t j = 0; j < num_cols; j++) {
            columnValues[j][i] = vertexValues[i][j];
        }
    }

    file.close();

		
        if (!columnValues.empty() && columnValues[0].size() != (L * M)) {
			fprintf(stderr, "Invalid number of values in the text file.\n");
			return 0;
		}
	
	
	do{
		for (const auto& row : columnValues) {
    
			// Clear the screen
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// Use our shader
			glUseProgram(programID);

            glm::mat4 Projection = glm::perspective(glm::radians(fov), 4.0f / 3.0f, 0.1f, 100.0f);

            // Recalcular la matriz MVP con el zoomLevel
            glm::mat4 MVP = Projection * View * Model;

			// Send our transformation to the currently bound shader,
			// in the "MVP" uniform
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

			std::string filename = "coordenadas.txt";

			mesh.updateYFromTextFile(row);

            float maxHeight = 2.3f; // Calcula la altura máxima actual de la malla
            float minHeight = 1.0f; // Calcula la altura mínima actual de la malla

            // Send the updated maxHeight and minHeight to the vertex shader
            GLuint maxHeightID = glGetUniformLocation(programID, "maxHeight");
            glUniform1f(maxHeightID, maxHeight);

            GLuint minHeightID = glGetUniformLocation(programID, "minHeight");
            glUniform1f(minHeightID, minHeight);

			// Render the mesh
			mesh.draw(programID);

			// Swap buffers
			glfwSwapBuffers(window);
			glfwPollEvents();

			// std::this_thread::sleep_for(std::chrono::milliseconds(50));
		}
    
	}
	 // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	glDeleteProgram(programID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
