#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo
#include "ShallowWater/topografia_uni.hpp"
#include "ShallowWater/curva_gaussiana_uni.hpp"
#include "ShallowWater/unidimensional.hpp"

// aceleracion gravitacional en la tierra m2/s
const float G = 9.80665;


namespace fs = std::filesystem;
using namespace std;



struct OutOfCell: std::logic_error {
  using logic_error::logic_error;
  OutOfCell(): logic_error("Index out of available cells"){}
};

int obtenerNumeroAleatorio(int N) {
    // Creamos un generador de números aleatorios utilizando el motor mersenne_twister_engine
    // como generador de números pseudoaleatorios.
    std::random_device rd;
    std::mt19937 gen(rd());

    // Creamos una distribución uniforme que generará números enteros en el rango de 0 a N-1.
    std::uniform_int_distribution<> distribucion(0, N - 1);

    // Generamos el número aleatorio utilizando la distribución y el generador.
    int numeroAleatorio = distribucion(gen);

    return numeroAleatorio;
}


ShallowWaterUnidim::ShallowWaterUnidim(
				   float step, 
				   float delta_time, 
				   fs::path path, 
				   float amplitud, 
				   float h_base=100.0):base(path){
	 this->delta_time = delta_time;
	 this->step = step;
	 this->topografia = base.genera_topografia(step);
	 // distancia de x_min a x_max + delta
	 this->length =  std::abs(topografia.last() - topografia.first());
	 // consider a position for interface and center
	 // left border : center : right border
	 this->N = topografia.length();
	 this->Points = 2*N+1;
	 this->line = new float[Points];
	 float max_topo = topografia.maxZ();

	 for (int index=0; index<Points;index++){
	   line[index] = max_topo + h_base;
	 }
	 // leer topografía
	 // calcular cada punto
	 int centro = 4;//obtenerNumeroAleatorio(Points);
	 int desviacion = 1;//obtenerNumeroAleatorio((int)Points/2);
	 // curva gaussiana: Points: 2*N + 1 (bordes y celdas)
	 estimulo = CurvaGaussiana(Points, centro, desviacion, amplitud);
	 partial = false;
	 /*
	   line: Points
	   u_vel : Points
	   topografia: N
	  */
   }
  

  /* constructor de build next u*/
ShallowWaterUnidim::ShallowWaterUnidim(
					 float* new_h, 
					 ShallowWaterUnidim& before):partial(true) {
    line = new float[before.getSize()];
	for (int i = 0; i < before.getSize();i++){
	  line[i] = new_h[i];
	}
	estimulo = before.estimulo;
	base = before.base;
	topografia = before.topografia;
	h_base = before.h_base;
	step = before.step;
	delta_time = before.delta_time;
	N = before.N;
	length = before.length;
	Points = before.Points;
	/*
	  Carga todo sin curva de velocidades
	 */
  }

void ShallowWaterUnidim::setEstimulo(CurvaGaussiana estimulo){
  /*
	Carga curva de velocidades
   */
	estimulo = estimulo;
	partial = false;
  }


void ShallowWaterUnidim::check_cell(int i) { 
	  if ( !(0<=i && i<Points) ){
		throw OutOfCell();
	  }
  }

int ShallowWaterUnidim::getCenter(int i){
	check_cell(i);
	int j = 2*i + 1;
	return j;
  }

int ShallowWaterUnidim::getLeft(int i){
	check_cell(i);
	int j = 2*i;
	return j;
  }


int ShallowWaterUnidim::getRight(int i){
	check_cell(i);
	int j = 2*i + 2;
	return j;
  }

int ShallowWaterUnidim::fixIndex(int i){ 
	return i;
  }

ShallowWaterUnidim::~ShallowWaterUnidim(){
  }


float ShallowWaterUnidim::velocidad(int i){
	return estimulo.getValor(i);
  }

float ShallowWaterUnidim::altura(int i){
	return line[i];
  }

int ShallowWaterUnidim::getSize() {
	return Points;
  }

float ShallowWaterUnidim::elapsed(int iteration){
	return (float)iteration * delta_time;
  }

float ShallowWaterUnidim::z(int i){
	return this->topografia.getValue(i);
  }

float ShallowWaterUnidim::u(int j){
	return this->estimulo.getValor(j);
  }

float ShallowWaterUnidim::h_ipos_n(Posicion pos, int j){
	/*
	  Parte -B- de ecuación 1.7
	  ipos =  i + 1/2 o i - 1/2
	  j :  getCenter(i <= Nx-1) <= 2Nx-1 
	  
	 */
    int pos_j = j;
	switch(pos) {
	  case Posicion::left: {
		//cout<<"H-ipos-n left velocidad en i:"<<pos_j-1<<endl;
		float v = velocidad(pos_j - 1);
		if (v>=0){
		  //cout << "Altura index "<<pos_j<<endl;
		  return altura(pos_j);
		} else {
		  pos_j -= 2;
		  if (pos_j==-1){
			pos_j = getCenter(N);
		  }
		  //cout << "Altura index "<<pos_j<<endl;
		  return altura(pos_j);
		}
	  }
	  case Posicion::right: {
		//cout<<"H-ipos-n right velocidad en i:"<<pos_j + 1<<endl;
		// obtener en tiempo 'n'
		float v = velocidad(pos_j + 1);
		if (v>=0){
		  //cout << "Altura index "<<pos_j<<endl;
		  return altura(pos_j);
		} else {		  
		  pos_j += 2;
		  if (pos_j == getCenter(N)+2){
			pos_j = getCenter(0);
		  }
		  //cout << "Altura index "<<pos_j<<endl;
		  return altura(pos_j);
		}
	  }
	  default: cout<<"No classification for this position"<<endl; break;
	}
	return 0.0;
  }
  
float ShallowWaterUnidim::q_ipos_n(Posicion pos, int j){
	/*

	  Parte -A- de ecuacion 1.7
	  Depende de -B-

	  ipos :: cell position in all array
	  j :  getCenter(i <= Nx-1) <= 2Nx-1: 1 ... 2Nx-1 

	 */
	//cout<<"Calculando h-ipos-n para pos_j:"<<j<<endl;
	float _h_ipos_n = h_ipos_n(pos, j);
	float _u_ipos_n = 0.0;

	int pos_j = 0;
	switch(pos) {
	  case Posicion::left: {
		// obtener en tiempo 'n'
		pos_j = j - 1;
		break;
	  }
	  case Posicion::right: {
		pos_j = j + 1;
		break;
	  }
	  default: break;
	}
	//cout<<"LAST: Calculando q-i-pos-n para pos_j:"<<pos_j<<endl;
    _u_ipos_n = velocidad(pos_j);
	//cout<<"LAST: velocidad:"<<_u_ipos_n<<endl;

	return _h_ipos_n * _u_ipos_n;
  }

float ShallowWaterUnidim::h_i_next(int center) {
	/*
	  Balance de masa (eq 1.6)
	 */
	Posicion left = Posicion::left;
	Posicion right = Posicion::right;
	float qr = q_ipos_n(right, center);
	float ql = q_ipos_n(left, center);
	cout <<"qr "<<qr<<" ql "<<ql<< " diff " <<qr-ql<<endl;
	return altura(center) - (delta_time/step) *  (qr-ql);
  }
  


  /*
	Ecuaciones para balance de masa (1.8)
   */

float ShallowWaterUnidim::q_i_n(int center){
	/*
	  obtener el punto medio de q en celda i (el centro), dados los bordes
	 */
	Posicion left = Posicion::left;
	Posicion right = Posicion::right;
	
	/*
	  if center==Points
	 */
	if (center==Points){
	  center = 1;
	}
	//cout <<"just q_i-n"<<endl;
	float qr = q_ipos_n(right, center);
	float ql = q_ipos_n(left, center);

	//cout <<"qr "<<qr<<" ql"<<ql<<endl;
	return (0.5)*(qr+ql);
  }

float ShallowWaterUnidim::uh_i_n(int center) {
	/*
	  Obtencion de la velocidad, en que dado el valor de q en el
	  centro (i), se obtiene el valor de la velocidad en un borde u
	  otro
	  u_hat_ en centro de celda, hay que tener el valor q_i
	 */
	//cout << "Index pre quin ->"<<i<<endl;
	float qin = q_i_n(center);

	//cout << "uh_i_n qin value "<< qin<<" index center" << center<<" Points "<<Points<<endl;
	if (qin>=0){
	  return velocidad(center - 1);

	}else {
	  //cout<<"Quin < 0"<<qin<< " i-> "<<i<<endl;
	  return velocidad(center + 1);
	}
  }

float ShallowWaterUnidim::getLength(){
  return length;
}

float ShallowWaterUnidim::nCells() {
  return N;
}

float ShallowWaterUnidim::h_imid(int center, int center_next) {
	/*
	  Obtiene la altura en la interfaz entre las dos celdas
	  Entrega centros de celda
	  i: {0...Nx}
	  i_next: {0...Nx}
	 */

	if (center==-1){
	  center = getCenter(N-1);
	}
	
	//cout<<"A Altura para index en "<<center<<endl;

	float h_i_n = altura(center);

	/*
	  condicion de borde h0 = hNx
	 */
	if (center_next==getCenter(N-1) + 2){
	  center_next = 1;
	}

	//cout<<"B Altura para index en "<<center_next<<endl;

	float h_inext_n = altura(center_next);

	return (0.5) * (h_i_n + h_inext_n);
  }
  
float ShallowWaterUnidim::u_i_next(int i_right, ShallowWaterUnidim& next_step){
	/*
	  Balance de momentum eq (1.8)
	  Despejando u_imid_next ->
	  Utiliza shallowwater cargado solo con la altura

	  i = {0...<N}
	 */ 


	int i_center = i_right  - 1;
	int i_next = i_right  + 1;
	if (i_next==Points){
	  i_next = 1;
	}
	// valor next para n+1
	// calcular de balance de masa:

	//cout<<"Calculando h_imid con next_step"<<endl;
	// cout<<*this<<endl;

	float _h_i_next = next_step.h_imid(i_center, i_next); // ok altura en interfaz


	//cout<<"Calculando h_imid_next ok"<<endl;

	// calcular los valores h_mid
	// toma h de la celda, y de la celda proxima
	// en array se salta 1, gracias a getCenter(i+1)
	float _h_i_n = h_imid(i_center, i_next); // ok altura en inferfaz

	// encontrar U_{i+1/2}_n

	// valor base velocidad:
	// en el borde a la derecha

	//cout<<"Obteniendo velocidad en borde derecho"<< i_right <<endl;
	float u_iright_n = u(i_right); // ok verificado



	//cout<<"Obteniendo factor base"<< i_right <<"*"<<_h_i_n<<endl;
	float left_factor_base  = u_iright_n * _h_i_n;

	/* calculo final 
	   de ecucación de balance de momentum
	 */ 

	// delta/x
	float alpha = delta_time / step;

	// próxima celda:
	// factor q * u (i+1) en siguiente celda

	// en el centro de la proximoa celda
	// se entrega el j de ese centro
	//cout << "i-center "<<i_center<<" Calculando q_i_n next cell "<<i_next<<endl;
	float q_inext_n = q_i_n(i_next);

	//cout <<" Calculando _uh_i_n next cell"<<i_next<<endl;
	float _uh_inext_n = uh_i_n(i_next);


	// en el centro de esta celda
	// factor q * u (i, n) en celda actual
	//cout <<" Calculando q_i_n center cell "<<i_center<<endl;
	float this_q_i_n = q_i_n(i_center);

	//cout <<" Calculando _uh_i_n center cell"<<i_next<<endl;
	float this_uh_i_n = uh_i_n(i_center);

	// diferencia de estos factores:::
	float diff_q = q_inext_n*_uh_inext_n - this_q_i_n * this_uh_i_n;
	// factores H
	// efecto grav y topografia:
	// elevacion en siguiente paso
	// usando la altura del siguiente paso en esa misma posicion
	float h_inext = next_step.altura(i_next);
	float h_i = next_step.altura(i_center);

	// h en celda interfaz a la dercha

	// factores de gravedad y topografia
	float efecto_grav_1 = (0.5) * G * (h_inext - h_i) * (h_inext + h_i);
	float efecto_grav_2 =  G * _h_i_next *(z(i_next) - z(i_center)); 
	float efecto_grav = efecto_grav_1 + efecto_grav_2;


	// factor del alfa
	float right_factor = alpha * (diff_q + efecto_grav);
	// calculo proximo u en i+1/2
	float u_next_iright = (1/_h_i_next) * (left_factor_base - right_factor); 
	return u_next_iright;
  }
  
void ShallowWaterUnidim::showInfo(){
	cout<<"N-> "<<N<<endl;
	cout<<"Points-> "<<Points<<endl;
	cout<<"Topografia len "<<topografia.length()<<endl;
  }

void ShallowWaterUnidim::newHeights(float* new_h){
	// line (altura) tiene Points elements
	int i_now = 0;
	int i_left = 0;
	int i_before = 0;

	int counter = 0;
	//showInfo();
	for (int i=0; i<N; i++){
	  i_now = getCenter(i);
	  i_left = i_now - 1;
	  i_before = i_now - 2;
	  // cout<<"pre h_imid"<<endl;
	  // showInfo();
	  // cout<<"i_left"<<i_left<<endl;
	  //cout<<"Iteracion  "<<i<<"hmid before"<<i_before<<" now "<<i_now<<endl;
	  new_h[i_left] = h_imid(i_before, i_now);
	  if (new_h[i_left]<0){cout<<"Mal calculado h-imid <0"<<new_h[i_left]<<endl;}
	  counter++;

	  // cout<<"pre h_i_next"<<endl;
	  // showInfo();
	  //cout<<"h-i-next .:: i_left"<<i_left<<endl;

	  
	  new_h[i_now] = h_i_next(i_now);
	  if (new_h[i_now]<0){cout<<"Mal calculado h-i-next <0"<<new_h[i_now]<<endl;}
	  counter++;
	}
	// i_now = Points - 2
	new_h[Points-1] = h_imid(i_now, 1);
	counter++;
	
	// for (int i=0; i<Points; i++){
	//   cout<<i <<" Iteracion "<<new_h[i]<<endl;
	// }
	
	//cout<<"Size new-h "<<counter<<" Old N "<<Points<<endl;
  }

void ShallowWaterUnidim::newVelocity(ShallowWaterUnidim& next_step, std::vector<float>& new_u){
	float value = estimulo.getValor(0);
	int i_right = 0;
	
	// in position index=1
	new_u.push_back(value);
	int i_center  =0 ;
	for (int i=0; i<N; i++){
	  // start inserting values in index: not {0,1} yes 2
	  // u_right
	  i_center = getCenter(i);
	  i_right = i_center + 1;

	  // cout<<"pre u_i_next"<<endl;
	  // showInfo();
	  //cout<<"i_right "<<i_right<<endl;

	  value = u_i_next(i_right, next_step);
	  //cout<<"Calculando u-next value :: "<<value<<endl;
	  new_u.push_back(value);

	  //i_right = getRight(i);
	  if (i_right<Points-1){
		//cout<<"Adding value to new_u after new-u"<<endl;
		value = estimulo.getValor(i_center);
		new_u.push_back(value);
		} // if i_right == Points -1 not add 0.0
	}
	// close the tail
	// in position index=0
	float last_value = new_u[Points-1];
	//cout<<"Calculando u-next value :: "<<value<<endl;
	new_u.insert(new_u.begin(),last_value);

	//cout<<"New velocity ok :)-> size new_u "<<new_u.size()<<endl;
  }

void  ShallowWaterUnidim::setEstimulo(CurvaGaussiana estimulo, std::vector<float> new_curva){

  this->estimulo = CurvaGaussiana(&estimulo, new_curva);

}

  /* generar siguiente iteración */
ShallowWaterUnidim ShallowWaterUnidim::next() {
	// generar nueva altura
	float* new_h =  new float[Points];	
	//cout<<"Calculando hnext en i"<<endl;
	newHeights(new_h);
	//cout<<"Creando new shallow water"<<endl;
    ShallowWaterUnidim next_step = ShallowWaterUnidim(new_h, *this);
	
	std::vector<float> new_u;
	newVelocity(next_step, new_u);

	// for (int i=0;i<Points;i++){
	//   cout<<"old_u "<< u(i) <<", new_U "<<new_u[i]<<endl;
	// }

	// u_i_next(int i,ShallowWaterUnidim& next_step)
	// set same curve before
	//cout<<"Set new curva"<<endl;

	next_step.setEstimulo(estimulo, new_u);


	// for (int i=0;i<Points;i++){
	//   cout<<"old_u "<< u(i) <<", new_U "<<next_step.u(i)<<"On curva "<<new_curva.getValor(i)<<endl;
	// }


	return next_step;
  }

void ShallowWaterUnidim::showTopografia(bool is_base){
	if (is_base==true){
	  cout<<base<<endl;
	} else {
	  cout<<topografia<<endl;
	}
  }

void ShallowWaterUnidim::showEstimulo() {
	  cout<<estimulo<<endl;
  }

std::vector<float> ShallowWaterUnidim::getX(){
	return topografia.getX();
  }

std::vector<float> ShallowWaterUnidim::getH(){
	std::vector<float> new_line;
	int i_center=1;
	for (int i=0;i<Points;i++){
	  new_line.push_back(line[i]);
	}
	return new_line;
  }

ShallowWaterUnidim& ShallowWaterUnidim::operator=(const ShallowWaterUnidim& other) {
	length = other.length;
	step = other.step;
	delta_time = other.delta_time;
	N = other.N;
	Points = other.Points;
	line = other.line;
	base = other.base;
	topografia = other.topografia;
	estimulo = other.estimulo;
	h_base = other.h_base;
	partial = false;
	return *this;
  }




std::ostream& operator<<(
						 std::ostream& os, 
						 const ShallowWaterUnidim& v) {
  os << "ShallowWaterUnidim(L:" << v.length << ",step:" << v.step << ",N:" << v.N <<",Points:"<<v.Points<<",delta time:"<<v.delta_time<<")";
  return os;
}
