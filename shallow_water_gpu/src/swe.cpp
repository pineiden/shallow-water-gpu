#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

void shallow_water_1d_test(int nx_init = 30);
void shallow_water_1d(int nx, int nt, double x_length, double t_length, double g, double** h_array, double** uh_array, double* x, double* t);
void boundary_conditions(int nx, int nt, double* h, double* uh, double t);
void initial_conditions(int nx, int nt, double* h, double* uh, double* x);
void write_data(int nx, int nt, double* x, double* t, double** h_array, double** uh_array);

int main() {
    std::cout << "Implementation of Shallow Water Equations in 1D" << std::endl;
    shallow_water_1d_test(30);
    return 0;
}

void shallow_water_1d_test(int nx_init) {
    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D_TEST:" << std::endl;
    std::cout << "  Compute a solution of the discrete shallow water equations" << std::endl;
    std::cout << "  over a 1-dimensional domain." << std::endl;

    int nx = nx_init;
    int nt = 500;
    double x_length = 1.0;
    double t_length = 0.5;
    double g = 9.8;

    double** h_array = new double*[nx];
    double** uh_array = new double*[nx];
    double* x = new double[nx];
    double* t = new double[nt + 1];
    for (int i = 0; i < nx; i++) {
        h_array[i] = new double[nt + 1];
        uh_array[i] = new double[nt + 1];
    }
    shallow_water_1d(nx, nt, x_length, t_length, g, h_array, uh_array, x, t);

    double x_min = *std::min_element(x, x + nx);
    double x_max = *std::max_element(x, x + nx);

    double h_min = 0.0;
    double h_max = h_array[0][0];
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            h_max = std::max(h_max, h_array[i][j]);

        }
    }

    double uh_min = 0.0;
    double uh_max = 0.0;
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            uh_min = std::min(uh_min, uh_array[i][j]);
            uh_max = std::max(uh_max, uh_array[i][j]);
        }
    }

    // Animation of H
    for (int it = 0; it < nt + 1; it++) {
        std::cout << "H(T), Step " << it << ", Time = " << t[it] << std::endl;
        // Perform OpenGL operations to display the plot
    }

    // Animation of UH
    for (int it = 0; it < nt + 1; it++) {
        std::cout << "UH(T), Step " << it << ", Time = " << t[it] << std::endl;
        // Perform OpenGL operations to display the plot
    }

    write_data(nx, nt, x, t, h_array, uh_array);
    
    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D_TEST:" << std::endl;
    std::cout << "  Normal end of execution." << std::endl;

    for (int i = 0; i < nx; i++) {
        delete[] h_array[i];
        delete[] uh_array[i];
    }
    delete[] h_array;
    delete[] uh_array;
    delete[] x;
    delete[] t;
}

void shallow_water_1d(int nx, int nt, double x_length, double t_length, double g, double** h_array, double** uh_array, double* x, double* t) {
    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D:" << std::endl;

    double* h = new double[nx];
    double* uh = new double[nx];
    double* hm = new double[nx - 1];
    double* uhm = new double[nx - 1];

    for (int i = 0; i < nx; i++) {
        x[i] = (double)i * x_length / (double)(nx - 1);
    }
    for (int i = 0; i < nt + 1; i++) {
        t[i] = (double)i * t_length / (double)nt;
    }
    double dx = x_length / (double)(nx - 1);
    double dt = t_length / (double)nt;

    initial_conditions(nx, nt, h, uh, x);

    boundary_conditions(nx, nt, h, uh, t[0]);

    for (int i = 0; i < nx; i++) {
        h_array[i][0] = h[i];
        uh_array[i][0] = uh[i];
    }

    for (int it = 1; it < nt + 1; it++) {
        for (int i = 0; i < nx - 1; i++) {
            hm[i] = (h[i] + h[i + 1]) / 2.0 - (dt / 2.0) * (uh[i + 1] - uh[i]) / dx;
            uhm[i] = (uh[i] + uh[i + 1]) / 2.0 - (dt / 2.0) * (uh[i + 1] * uh[i + 1] / h[i + 1] + 0.5 * g * h[i + 1] * h[i + 1] - uh[i] * uh[i] / h[i] - 0.5 * g * h[i] * h[i]) / dx;
        }

        for (int i = 1; i < nx - 1; i++) {
            h[i] = h[i] - dt * (uhm[i] - uhm[i - 1]) / dx;
            uh[i] = uh[i] - dt * (uhm[i] * uhm[i] / hm[i] + 0.5 * g * hm[i] * hm[i] - uhm[i - 1] * uhm[i - 1] / hm[i - 1] - 0.5 * g * hm[i - 1] * hm[i - 1]) / dx;
        }

        boundary_conditions(nx, nt, h, uh, t[it]);

        for (int i = 0; i < nx; i++) {
            h_array[i][it] = h[i];
            uh_array[i][it] = uh[i];
        }
    }

    std::cout << std::endl;
    std::cout << "SHALLOW_WATER_1D:" << std::endl;
    std::cout << "  Normal end of execution." << std::endl;

    delete[] h;
    delete[] uh;
    delete[] hm;
    delete[] uhm;
}

void boundary_conditions(int nx, int nt, double* h, double* uh, double t) {
    h[0] = h[nx - 2];
    h[nx - 1] = h[1];
    uh[0] = uh[nx - 2];
    uh[nx - 1] = uh[1];
}

void initial_conditions(int nx, int nt, double* h, double* uh, double* x) {
    for (int i = 0; i < nx; i++) {
        h[i] = 2.0 + sin(2.0 * M_PI * x[i]);
        uh[i] = 0.0;
    }
}

void write_data(int nx, int nt, double* x, double* t, double** h_array, double** uh_array) {
    std::ofstream file_x("sw1d_x.txt");
    std::ofstream file_t("sw1d_t.txt");
    std::ofstream file_h("sw1d_h.txt");
    std::ofstream file_uh("sw1d_uh.txt");

    for (int i = 0; i < nx; i++) {
        file_x << x[i] << std::endl;
    }
    for (int i = 0; i < nt + 1; i++) {
        file_t << t[i] << std::endl;
    }
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            file_h << h_array[i][j] << " ";
        }
        file_h << std::endl;
    }
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < nt + 1; j++) {
            file_uh << uh_array[i][j] << " ";
        }
        file_uh << std::endl;
    }

    file_x.close();
    file_t.close();
    file_h.close();
    file_uh.close();

    std::cout << std::endl;
    std::cout << "  X  values saved in file \"sw1d_x.txt\"" << std::endl;
    std::cout << "  T  values saved in file \"sw1d_t.txt\"" << std::endl;
    std::cout << "  H  values saved in file \"sw1d_h.txt\"" << std::endl;
    std::cout << "  UH values saved in file \"sw1d_uh.txt\"" << std::endl;
}

