#include <filesystem>
#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "ShallowWater/shaders.hpp"

namespace fs = std::filesystem;
using namespace std;


std::string readFile(fs::path path)
{
    // Open the stream to 'lock' the file.
    std::ifstream f(path, std::ios::in | std::ios::binary);

    // Obtain the size of the file.
    const auto sz = fs::file_size(path);

    // Create a buffer.
    std::string result(sz, '\0');

    // Read the whole file into the buffer.
    f.read(result.data(), sz);

    return result;
}


  
Shaders::Shaders(const std::string &vertex_path, const std::string &fragment_path) {
	this->vertexShader_path = std::filesystem::path("shaders/shaders") / vertex_path;
	this->fragmentShader_path = std::filesystem::path("shaders/shaders") / fragment_path;
	readShaders();
  }

void Shaders::readShaders(){
	this->vertexShader = readFile(this->vertexShader_path);
	this->fragmentShader = readFile(this->fragmentShader_path);
  }

void Shaders::show(){
	std::cout << this->vertexShader << std::endl;
	std::cout << this->fragmentShader << std::endl;
  }
  
const std::string& Shaders::fragment()  const{
	return this->fragmentShader;
  }

const std::string& Shaders::vertex() const{
	return this->vertexShader;
  }


const std::string& Shaders::fragment_path()  const{
	return this->fragmentShader_path;
  }

const std::string& Shaders::vertex_path() const{
	return this->vertexShader_path;
  }

