#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "ShallowWater/plane.hpp"

MeshPlane plane(std::vector<std::vector<double>>(), 10, 10, 1); 

void display() {
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    plane.updatePlane();
    glutSwapBuffers();
}

void reshape(int width, int height) {
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(45.0, static_cast<double>(width) / static_cast<double>(height), 0.1, 100.0);
}

void timer(int value) {
    plane.nextStep();
    glutPostRedisplay();
    glutTimerFunc(100, timer, 0);
}


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Mesh Plane");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, timer, 0);
    glEnable(GL_DEPTH_TEST);
    glutMainLoop();
    return 0;
}
