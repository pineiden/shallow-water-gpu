#+TITLE: Game of Life with CUDA GPU

Estructura de directorio


#+RESULTS:

#+begin_src bash
tree -d -L 2 
#+end_src

#+RESULTS:
| .   |             |              |
| ├── | build       |              |
| │   | ├──         | CMakeFiles   |
| │   | ├──         | extern       |
| │   | ├──         | src          |
| │   | └──         | tests        |
| ├── | extern      |              |
| │   | └──         | googletest   |
| ├── | include     |              |
| │   | └──         | ShallowWater |
| ├── | src         |              |
| └── | tests       |              |
|     |             |              |
| 11  | directories |              |



Inspeccionar archivos *src*

#+begin_src bash
ls src/*.cpp
#+end_src

#+RESULTS:
| src/grid.cpp |
| src/main.cpp |
| src/swe.cpp  |
| src/swe2.cpp |
