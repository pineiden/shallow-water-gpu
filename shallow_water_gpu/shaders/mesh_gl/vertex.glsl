#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;

out vec3 fragmentColor;
uniform mat4 MVP;
uniform float maxHeight; // Altura máxima de la malla
uniform float minHeight; // Altura mínima de la malla

void main(){  

    gl_Position =  MVP * vec4(vertexPosition_modelspace, 1);

    float heightRange = maxHeight - minHeight;
    
    float currentHeight = (vertexPosition_modelspace.y - minHeight) / heightRange;

    vec3 adjustedColor = mix(vec3(0.0, 0.0, 1.0), vec3(0.7, 0.7, 1.0), currentHeight);
    
    fragmentColor = adjustedColor;
}
