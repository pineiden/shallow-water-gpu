#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo
//#include "topografia_uni.hpp"
//#include "curva_gaussiana_uni.hpp"

enum Posicion {
  left=1, 
  center=2, 
  right=3
};

namespace fs = std::filesystem;
using namespace std;

// aceleracion gravitacional en la tierra m2/s
//const float G = 9.80665;

class ShallowWaterUnidim{
private:
  float length;
  float step;
  float delta_time;
  int N;
  int Points;
  float *line;
  // base topografica es la fuente
  TopografiaUnidim base;
  // es el array de puntos registrados de malla con su altura topografica
  TopografiaUnidim topografia;
  // va a representar la velocidad en el punto i
  CurvaGaussiana estimulo;
  float h_base;
  bool partial;
public:
  ShallowWaterUnidim(
				   float step, 
				   float delta_time, 
				   fs::path path, 
				   float amplitud, 
				   float h_base);

  
  /* constructor de build next u*/
  ShallowWaterUnidim(
					 float* new_h, 
					 ShallowWaterUnidim& before);


  void setEstimulo(CurvaGaussiana estimulo);

  void check_cell(int i);

  int getCenter(int i);

  int getLeft(int i);

  int getRight(int i);

  int  fixIndex(int i);
  
  void  setEstimulo(CurvaGaussiana estimulo, std::vector<float> new_curva);


  ~ShallowWaterUnidim();

  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const ShallowWaterUnidim& v);

  float velocidad(int i);

  float altura(int i);

  int getSize();

  float elapsed(int iteration); 

  float z(int i);

  float u(int i);

  float h_ipos_n(Posicion pos, int i);
  
  float q_ipos_n(Posicion pos, int i);

  float h_i_next(int center);


  /*
	Ecuaciones para balance de masa (1.8)
   */

  float q_i_n(int i);

  float uh_i_n(int i);

  float h_imid(int i, int i_next); 

  float u_i_next(int i_right, ShallowWaterUnidim& next_step);
  
  void showInfo();

  void newHeights(float* new_h);

  void newVelocity(ShallowWaterUnidim& next_step, std::vector<float>& new_u);

  /* generar siguiente iteración */
  ShallowWaterUnidim next();

  void showTopografia(bool is_base);

  void showEstimulo();
  
  float getLength();

  float nCells();

  std::vector<float> getX();

  std::vector<float> getH();

  ShallowWaterUnidim& operator=(const ShallowWaterUnidim& other);
};
