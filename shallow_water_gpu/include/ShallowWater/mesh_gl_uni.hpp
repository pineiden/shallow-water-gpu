#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#ifndef _INCL_MESH_UNI_GUARD
#define _INCL_MESH_UNI_GUARD
namespace fs = std::filesystem;
using namespace glm;


class Mesh {
public:
    Mesh(int L, int M, float step);

    void initializeBuffers();
    void setupVertexAttribs(GLuint programID);

	~Mesh(); 
    void draw(GLuint programID); 
    void updateY(std::vector<float> vertexValues);
private:
    int L_;                  // Largo de la malla (número de filas)
    int M_;                  // Ancho de la malla (número de columnas)
    float step_;             // Separación entre vértices
    int numVertices_;        // Número total de vértices
    int numIndices_;         // Número total de índices
    std::vector<vec3> vertices_;    // Vector de vértices
    std::vector<vec3> colors_;      // Vector de colores
    std::vector<unsigned int> indices_; // Vector de índices
    GLuint vertexArrayID_;   // ID del Vertex Array Object (VAO)
    GLuint vertexbuffer_;    // ID del buffer de vértices
    GLuint colorbuffer_;     // ID del buffer de colores
    GLuint elementbuffer_;   // ID del buffer de índices
};


#endif
