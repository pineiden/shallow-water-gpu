#include <filesystem>
#include <iostream>
#include <string>
#include <vector>


#ifndef _INCL_SHADERS_GUARD
#define _INCL_SHADERS_GUARD


namespace fs = std::filesystem;
using namespace std;

class Shaders {
private:
  fs::path vertexShader_path;
  fs::path fragmentShader_path;
  std::string vertexShader;
  std::string fragmentShader;
public:
  Shaders(
		  const std::string &vertex_path, 
		  const std::string &fragment_path);

  void readShaders();
  void show();
  
  const std::string& fragment()  const;

  const std::string& vertex() const;

  const std::string& fragment_path()  const;

  const std::string& vertex_path() const;
};

#endif
