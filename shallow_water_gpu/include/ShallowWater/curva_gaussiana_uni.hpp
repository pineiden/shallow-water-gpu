#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>
#include <random>
#include <algorithm> // Incluimos la biblioteca algorithm para usar la función std::max_element
#include <chrono> // Para trabajar con tiempo


class CurvaGaussiana {
private:
    int N; // Tamaño del array
    std::vector<float> datos; // Array de N floats para almacenar la curva gaussiana
    float amplitud; // Amplitud de onda

public:
    // Constructor que inicializa el array de N floats como una curva gaussiana con centro en i, desviación estándar de j celdas y amplitud de onda
  CurvaGaussiana(int size, int i, int j, float amplitud_onda);
  CurvaGaussiana(CurvaGaussiana* curva,std::vector<float>& new_datos);

  void setCurva(std::vector<float>& new_datos);
 
    // Destructor para liberar la memoria del array
  CurvaGaussiana();

    // Destructor para liberar la memoria del array
  ~CurvaGaussiana();

    // Obtener el valor en una posición del array
  float getValor(int index) const;

  int getSize();

  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const CurvaGaussiana& v);

  float maxZ() const;

  CurvaGaussiana& operator=(const CurvaGaussiana& other);
};
