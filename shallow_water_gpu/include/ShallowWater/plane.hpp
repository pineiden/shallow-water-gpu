#include <vector>
#include <tuple>

#ifndef __PLANE_HPP
#define __PLANE_HPP

class MeshPlane {
private:
    int x;
    int y;
    double distance;
    std::vector<std::vector<double>> H;
    int current_h;
    std::vector<std::vector<double>> vertices;
    std::vector<std::pair<int, int>> edges;
    std::vector<std::tuple<int, int, int>> surfaces;
    std::vector<std::tuple<float, float, float>> colors;

public:
    MeshPlane(std::vector<std::vector<double>> init_cond, int x, int, double distance);

    int getCurrentH() const;

    void setCurrentH(int a);

    std::vector<std::vector<double>> getH() const;

    void setH(std::vector<std::vector<double>> a);

    int getX() const;

    void setX(int a);

    int getY() const;

    void setY(int a);

    double getDistance() const;

    void setDistance(double a);

    void nextStep();

    void applyCurrentH();

    void initializeStuff();

    void updatePlane();

};
#endif
