#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>
#include <stdexcept>
//#include "<cxxopts.hpp>"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <memory>

using TupleKey = std::pair<int, int>;
namespace fs = std::filesystem;
using namespace std;

struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        // Mainly for demonstration purposes, i.e. works but is overly simple
        // In the real world, use sth. like boost.hash_combine
        return h1 ^ h2;  
    }
};


class TopografiaUnidim {
private:
  fs::path path;
  std::vector<std::pair<float, float>> height_position;
  std::unordered_map<TupleKey, float, pair_hash> pendientesMap;
  size_t size;
public:
  TopografiaUnidim();

  TopografiaUnidim(std::vector<std::pair<float, float>> hp);


  TopografiaUnidim(fs::path path);

  std::vector<float> getX();

  float height(float x);

  float pendiente(int i, int j);

  float recta(int i, int j, float x);

  float first();

  float last();

  void addPendiente(int i, int j, float pendiente);

  // Obtener la pendiente entre las tuplas (i, j)
  float getPendiente(int i, int j);

  friend std::ostream& operator<<(
						   std::ostream& os, 
						   const TopografiaUnidim& v);

  TopografiaUnidim genera_topografia(float step);
  
  int length();

  float getValue(int i);

  float maxZ() const;

};

